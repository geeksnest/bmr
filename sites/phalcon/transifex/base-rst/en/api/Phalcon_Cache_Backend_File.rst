%{Phalcon_Cache_Backend_File_fff9a66e44d0015bd43dc59901202dc2}%
=======================================

%{Phalcon_Cache_Backend_File_797e5d789434d10ddd0a11dc0d3a9e57|:doc:`Phalcon\\Cache\\Backend <Phalcon_Cache_Backend>`}%

%{Phalcon_Cache_Backend_File_c1eac5b56048ee7e86f6a6737e5ede2a|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Cache_Backend_File_7f883b36cf009dfc151d75505e0ea095}%

.. code-block:: php

    <?php

    //{%Phalcon_Cache_Backend_File_9175465ecad7e09da0c1cb6bf3ef8f42%}
    $frontendOptions = array(
    	'lifetime' => 172800
    );
    
      //{%Phalcon_Cache_Backend_File_35fa2306c2b57a1793866772fcd5b29a%}
      $frontCache = \Phalcon\Cache\Frontend\Output($frontOptions);
    
    //{%Phalcon_Cache_Backend_File_0b3b82cd850fb00c9bccb774526464f4%}
    $backendOptions = array(
    	'cacheDir' => '../app/cache/'
    );
    
      //{%Phalcon_Cache_Backend_File_4cdcda0b6c4b68f0ad91a5047210bc8d%}
      $cache = new \Phalcon\Cache\Backend\File($frontCache, $backendOptions);
    
    $content = $cache->start('my-cache');
    if ($content === null) {
      	echo '<h1>', time(), '</h1>';
      	$cache->save();
    } else {
    	echo $content;
    }




%{Phalcon_Cache_Backend_File_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Backend_File_d0ebe8723e443cd9102587c6adecc491|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Backend_File_fcea80a854c773de18acba0474dce07b}%

%{Phalcon_Cache_Backend_File_051aecfbd55511e7172defa011547cfe}%

%{Phalcon_Cache_Backend_File_3bff3193b42b39b97ac714131c9e7a9c}%

%{Phalcon_Cache_Backend_File_b7167b7aa44b968e246ab55a957dc5ae}%

%{Phalcon_Cache_Backend_File_8c5e3867b08984611c66e171b24139cc}%

%{Phalcon_Cache_Backend_File_92a86099a531589aac19438e058b04bf}%

%{Phalcon_Cache_Backend_File_5a3c26a229b51db139ac1ff3258ce404}%

%{Phalcon_Cache_Backend_File_d073186049f74018e6ac389752e0231d}%

%{Phalcon_Cache_Backend_File_0496bd63d7dd49a90bc039bf152277c2}%

%{Phalcon_Cache_Backend_File_1802823d299420e98af3a958b7d763e9}%

%{Phalcon_Cache_Backend_File_33dca6e3f0f50fd099a0f3307d79d852}%

%{Phalcon_Cache_Backend_File_51b9f069d953ee0838b4029d1623b43f}%

%{Phalcon_Cache_Backend_File_0e8880af8ead34ea21563440008906ce}%

%{Phalcon_Cache_Backend_File_458a4b4698c693cac55078739b14aecd}%

%{Phalcon_Cache_Backend_File_ab27e2da0452b8fdddf3ce3f31ddc8a0}%

%{Phalcon_Cache_Backend_File_243f7860634f5addc7cc2cd15ec9587b}%

%{Phalcon_Cache_Backend_File_5fbdd81239a6a360b99505e1ccc4c96a}%

%{Phalcon_Cache_Backend_File_4c1e2a7813fc357bd5b2961fe618cf57}%

%{Phalcon_Cache_Backend_File_5aa5eebbc6e2e0ba1eb669cfcf9314ac}%

%{Phalcon_Cache_Backend_File_f8bbbf10eb7b23d1ad6555244df95daa}%

%{Phalcon_Cache_Backend_File_ee84c5ddac90bc82f87d82edd83f803d}%

%{Phalcon_Cache_Backend_File_20b1a896cfa41a581678813a40ad294b}%

%{Phalcon_Cache_Backend_File_a45d0bbd7f1d2ca9be308e066116571d}%

%{Phalcon_Cache_Backend_File_f4c67e395756ed811b3d86ccd37680d7}%

%{Phalcon_Cache_Backend_File_0a7c1cf9fc6cf16b0c6d365b453b8c9c}%

%{Phalcon_Cache_Backend_File_6e3b53a9e2f02ef15c6e622b43ad7fd6}%

%{Phalcon_Cache_Backend_File_2e2e8a1edb8ee6d249c3408ed9a13506}%

%{Phalcon_Cache_Backend_File_f616a41f5f598297276e1c7ec3a0c216}%

%{Phalcon_Cache_Backend_File_132cc48b9d06f81662724fafbcf5388d}%

%{Phalcon_Cache_Backend_File_25608a5b33db734ab297a041c17d0afe}%

%{Phalcon_Cache_Backend_File_4f309f2cbbf78c9cc272007b4b90e759}%

%{Phalcon_Cache_Backend_File_d95154d7678637473d5bd3651a6b7981}%

%{Phalcon_Cache_Backend_File_63ef827e599ff5e9ce28cac18e0a74de}%

%{Phalcon_Cache_Backend_File_8f54d16975ae8256cc1f314641e21364}%

%{Phalcon_Cache_Backend_File_7b222737002802a81ee0a69bdb497f3a}%

