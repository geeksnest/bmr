%{Phalcon_Filter_2219f942c6168e935dc4dc268ff4dcfd}%
=========================

%{Phalcon_Filter_f2bc0cc5ac3922369ca2ab15ecb1b14f|:doc:`Phalcon\\FilterInterface <Phalcon_FilterInterface>`}%

%{Phalcon_Filter_c6b1093b7758231ad00dc64df86ba5a1}%

.. code-block:: php

    <?php

    $filter = new Phalcon\Filter();
    $filter->sanitize("some(one)@exa\\mple.com", "email"); // {%Phalcon_Filter_d5821a95e7aaa55ed43b72ef0e14775c%}
    $filter->sanitize("hello<<", "string"); // {%Phalcon_Filter_8f053e796ba85cac6d9b8898fa350974%}
    $filter->sanitize("!100a019", "int"); // {%Phalcon_Filter_7b16cae913f7b66b3cede60e5a001159%}
    $filter->sanitize("!100a019.01a", "float"); // {%Phalcon_Filter_75dbb39ae8c53bfa2516d1529db41588%}




%{Phalcon_Filter_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Filter_fd3f9c09ab7fa73b13ce9022b279b2e3|:doc:`Phalcon\\Filter <Phalcon_Filter>`}%

%{Phalcon_Filter_bfff449a04781daf971d8f409529e79e}%

%{Phalcon_Filter_d11f7b4f6e965ecc7d5091e24840bd35}%

%{Phalcon_Filter_bfb96e60086cb4c3cf760efe0f334b16}%

%{Phalcon_Filter_33a9cc93212585a6d3bcc82583c243f9}%

%{Phalcon_Filter_aa31399dbda8187cb4e7f166c24b6509}%

%{Phalcon_Filter_6e7dc8e82b465d8c655af5c97ac0f03e}%

%{Phalcon_Filter_9e163e0029b2562e5784934ccea439ab}%

