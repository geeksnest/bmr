%{Phalcon_Db_1676b071bd632103b0c86d05f5b68b88}%
==============================

%{Phalcon_Db_0942707debec359ef1763ba7a34572b0}%

.. code-block:: php

    <?php

    try {
    
      $connection = new Phalcon\Db\Adapter\Pdo\Mysql(array(
         'host' => '192.168.0.11',
         'username' => 'sigma',
         'password' => 'secret',
         'dbname' => 'blog',
         'port' => '3306',
      ));
    
      $result = $connection->query("SELECT * FROM robots LIMIT 5");
      $result->setFetchMode(Phalcon\Db::FETCH_NUM);
      while ($robot = $result->fetch()) {
        print_r($robot);
      }
    
    } catch (Phalcon\Db\Exception $e) {
    echo $e->getMessage(), PHP_EOL;
    }




%{Phalcon_Db_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Db_c0312ef262cfeadad1586b5aee183454}%

%{Phalcon_Db_bd70df84bfce70fe85a5e91c444b94e0}%

%{Phalcon_Db_efb0b86861853defc3bfd7d1f1e49ada}%

%{Phalcon_Db_6a5342a1716582f50feab5ca11af3945}%

%{Phalcon_Db_eb14492ee7effef4ee398c6355bfe816}%

%{Phalcon_Db_43d7ac8636a9f13c88b71948cd200b01}%

%{Phalcon_Db_eb1a58864e4d153292e3b9e818f312d7}%

%{Phalcon_Db_45bcd6075657c6b0dee5d9e278264a08}%

%{Phalcon_Db_71c9dbc62ea5257a20311014949588d6}%

%{Phalcon_Db_cf6be583f38dbf16758132361af0ed1f}%

%{Phalcon_Db_9822271f8fbfc900691d41868057c25b}%

%{Phalcon_Db_4a4583dd4116faa0403ab53b5c2b492b}%

%{Phalcon_Db_b5f4cba9718f3d7b776bf65804fd19f6}%

%{Phalcon_Db_1e64d3d620034f22b0236f936d059246}%

%{Phalcon_Db_39efece7a5e2ae848f507fe93ea6c94c}%

%{Phalcon_Db_4a46efd584f2ff62e973a3a610bcf485}%

%{Phalcon_Db_0cd8098409466426404aff430faa1bc3}%

%{Phalcon_Db_e0ae3ddc4f13e9592d9ac473eea064b2}%

%{Phalcon_Db_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_ec82c8b3088594c9db6a3c25f726cbba}%

%{Phalcon_Db_d6b490bfe18d0d1094d8752966634f72}%

