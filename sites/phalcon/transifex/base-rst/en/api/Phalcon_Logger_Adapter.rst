%{Phalcon_Logger_Adapter_c238c61851e673d703589fbef297d7fe}%
===========================================

%{Phalcon_Logger_Adapter_06555c5c54842ff80b30f63285905d43|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_bf0117c4b78aa481eb72f5b7c4e6ea7b}%

%{Phalcon_Logger_Adapter_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Logger_Adapter_68d20d640164c4f7bdba2a158e81e961|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_7179b7a0bdedce8790bec183015cd827}%

%{Phalcon_Logger_Adapter_74a07759122a15d3033bd8add3ff820b}%

%{Phalcon_Logger_Adapter_b7348a52ab6dd55a4e5fb2b7d3ba889e}%

%{Phalcon_Logger_Adapter_d67ca3dd67f883ff340807c013066f5f|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`|:doc:`Phalcon\\Logger\\FormatterInterface <Phalcon_Logger_FormatterInterface>`}%

%{Phalcon_Logger_Adapter_5b5292c115a695fb23679317ba51b8b1}%

%{Phalcon_Logger_Adapter_5a065059c1775aefbeaae79f7f9e5577|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_26c40cdf4c3b0d59d1dd64f038e0d9a0}%

%{Phalcon_Logger_Adapter_d72b6fd7a2c1435504cc8f70fe25c528|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_ae86336403bef4c94abd68876f8583ab}%

%{Phalcon_Logger_Adapter_fa026d825dbe238fe1d80f1575c2d796|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_974c1f35cd09f2112bea68c83c0cb09e}%

%{Phalcon_Logger_Adapter_a4c5b25c79c4970538b3b49e7d6019c1|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_7658a187f1ddda6c757e1ccbc6b0a3b4}%

%{Phalcon_Logger_Adapter_12efe98741b94e04553ab8d204a44448}%

%{Phalcon_Logger_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Logger_Adapter_54932f1b2f3dae819ee33d2f7a246272|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_f55eae329bf25a51595453ecc14d268a}%

%{Phalcon_Logger_Adapter_ae4b8b1c8271c21a35fa573b9638218b|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_41fb655515dd67d271d1668b99ae62c8}%

%{Phalcon_Logger_Adapter_16ac80b761ad9940dd66e662e8943c33|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_b547549878361a06f0705a88d13c30d1}%

%{Phalcon_Logger_Adapter_f2c090de4536bd65ebc6bab1812d7785|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_ba89f314766499b7979510665f4b7ac1}%

%{Phalcon_Logger_Adapter_c37fc8b5214d6198f4ce074167c680e0|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_1ddbc6e136173b1e10a397223b124fd1}%

%{Phalcon_Logger_Adapter_8655ff76f226d6c1840121754122db81|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_44f68c546de28aea3513d007f11708e8}%

%{Phalcon_Logger_Adapter_f51080d72bb792b1242c4e917a36941d|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_284089fdb19265df523bcdeb006099bf}%

%{Phalcon_Logger_Adapter_ab2825ac377e3f674e24e7de4dba2eb7|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_dc8fa89976b49d9fda57eb5f0ad47406}%

%{Phalcon_Logger_Adapter_aaad3c3ffb3254d699e7a44a532267d6|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_8f730ff32266166faffa41db189b5721}%

%{Phalcon_Logger_Adapter_9d62e454f88c1e1d62318c5be908e0d7}%

%{Phalcon_Logger_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Logger_Adapter_c630e9442bad243204b86bd847871bcf|:doc:`Phalcon\\Logger\\FormatterInterface <Phalcon_Logger_FormatterInterface>`}%

%{Phalcon_Logger_Adapter_d403d5f385f19f8b13752601556c4e51}%

%{Phalcon_Logger_Adapter_4d1f98799e5c52e6c99728237e74e78c}%

%{Phalcon_Logger_Adapter_b17425a56dfeebdb4fe4f995b044cc99}%

