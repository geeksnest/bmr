%{Phalcon_Mvc_View_06e8b1dadab37ca84d8bbde83d6df8c2}%
============================

%{Phalcon_Mvc_View_32f67d09941ad6b6963c440e030d031e|:doc:`Phalcon\\DI\\Injectable <Phalcon_DI_Injectable>`}%

%{Phalcon_Mvc_View_dab333b6b1b7d2def5cf9c49199a27e0|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Mvc\\ViewInterface <Phalcon_Mvc_ViewInterface>`}%

%{Phalcon_Mvc_View_9c317149dc307ab7c9d23ac0282f33b2}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_View_fef0d68872451f7ff922cc04ffc25648%}
     $view = new Phalcon\Mvc\View();
     $view->setViewsDir('app/views/');
    
     $view->start();
     //{%Phalcon_Mvc_View_ae3151340a5be99b85680be73edcfea8%}
     $view->render('posts', 'recent');
     $view->finish();
    
     //{%Phalcon_Mvc_View_87fe7fd6b0356d0f456c79df0ce8e91c%}
     echo $view->getContent();




%{Phalcon_Mvc_View_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_View_fbe05011b248c36db9264fa3d78587f2}%

%{Phalcon_Mvc_View_48fd30db0c702ff1166ccbdebc32890c}%

%{Phalcon_Mvc_View_c52227e1c92e1969bfa363bf09a743cb}%

%{Phalcon_Mvc_View_900f7d3d96a087bd5abc64ceae736cc7}%

%{Phalcon_Mvc_View_f5ddb72a210fdfe698f95226886ed721}%

%{Phalcon_Mvc_View_3b0ef6256d8d587ecdb88f0b483fb551}%

%{Phalcon_Mvc_View_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_View_38d168e0be68a8fb5da8d35b168503c0}%

%{Phalcon_Mvc_View_43b61bfe2f34172cb8e4661eab2f2380}%

%{Phalcon_Mvc_View_b5429ce47d1c4183c2d61759bc11ad7a|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_f3bd3758c127a36f8c3c5f37fd1362e1}%

%{Phalcon_Mvc_View_a9db8035960f804122fab3edcdc26906}%

%{Phalcon_Mvc_View_5cb8356ba2a4cb3c42531d901b21cc6c}%

%{Phalcon_Mvc_View_ec259fcf9a5343b7888a43c09fe2c5de|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_550ccbbd1e13fcb448f3dce5e7da0a5e}%

.. code-block:: php

    <?php

     $view->setLayoutsDir('../common/layouts/');





%{Phalcon_Mvc_View_67051c20be91d849f4efe7986f04a210}%

%{Phalcon_Mvc_View_d6cd6bfee3aab4bd14ef320ddd812466}%

%{Phalcon_Mvc_View_ec9085086b730349a70fce2fab54d37e|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_6a5ab56e93f8f4a526e6e7874c80fd3b}%

.. code-block:: php

    <?php

     $view->setPartialsDir('../common/partials/');





%{Phalcon_Mvc_View_d38fd95894d532ae01c024bb49b11580}%

%{Phalcon_Mvc_View_b08488652feadf089374615891aeba88}%

%{Phalcon_Mvc_View_867e442a39927595b3c76ab399f5e936|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_8f58a325b6238247ae7e6dfc6c8edac5}%

.. code-block:: php

    <?php

     	$view->setBasePath(__DIR__ . '/');





%{Phalcon_Mvc_View_c545a37345827bb3b1217493b008dd35}%

%{Phalcon_Mvc_View_4acf5eccab35ab1531e5566a1dc07f4b}%

%{Phalcon_Mvc_View_979ac30be2b9b9ab963c7edaa5ef6d61}%

%{Phalcon_Mvc_View_4acf5eccab35ab1531e5566a1dc07f4b}%

%{Phalcon_Mvc_View_bb7e9598a19f7ea533dc6632f019bf63|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_e7d73fdb6f23fd4d44b558ae2370ee14}%

.. code-block:: php

    <?php

     	//{%Phalcon_Mvc_View_35be3ae532c9521666a84f400d986c4a%}
     	$this->view->setRenderLevel(View::LEVEL_LAYOUT);





%{Phalcon_Mvc_View_b386e75736bbe8d77b561449cbd67b63|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_3e557a9d9fc8dbffdc09e0da6111048d}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_View_8a4fd524b5ad0e2a5fd2a00355fcaa7a%}
     $this->view->disableLevel(View::LEVEL_ACTION_VIEW);





%{Phalcon_Mvc_View_e79f037c8a0a96439a119991123fdaa0}%

%{Phalcon_Mvc_View_bce091fe4b5a1d9d43e2796d1fb09bf0}%

%{Phalcon_Mvc_View_45be4c6cba49d63645d01fbff39b6cbc|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_63971b316ebf05efa3a4b2ba373392bf}%

.. code-block:: php

    <?php

     	//{%Phalcon_Mvc_View_f41c2a8c5bc29726f38c511e3e94fb45%}
     	$this->view->setMainView('base');





%{Phalcon_Mvc_View_44ddf5c66a6c24357741a75101f86ae6}%

%{Phalcon_Mvc_View_b609711443551aa96000f83854c5a68c}%

%{Phalcon_Mvc_View_a166a7c12e4a830f4cea48afb3f7fe2b|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_1d4c361a8b82541b9d2faf0eb12c1af0}%

.. code-block:: php

    <?php

     	$this->view->setLayout('main');





%{Phalcon_Mvc_View_3e22ac2b575d532f299125778552875c}%

%{Phalcon_Mvc_View_b609711443551aa96000f83854c5a68c}%

%{Phalcon_Mvc_View_94b9349d0efb15cbb57ef36c69e5b75b|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_7c04556d75c98552281bf763c7342eca}%

%{Phalcon_Mvc_View_d9307fedd12925b5886b0a13d2f7501e|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_ab26b6f9bbbbde091dc38d3cd3bbb2aa}%

%{Phalcon_Mvc_View_879e91639b848b52799215ed4f430de3|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_69314476c85a898be6cfb0819921dbef}%

%{Phalcon_Mvc_View_7578a9869a6d96d63ae63e587c438073|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_61bf38948b5a29a58af7dbc12f57aeb6}%

%{Phalcon_Mvc_View_43e408ac6077e34be527c8dd78bfc335|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_5666eb7f8db16aa0f9cb46e2cd45b1f5}%

.. code-block:: php

    <?php

    $this->view->setParamToView('products', $products);





%{Phalcon_Mvc_View_1c871b3f919310e68e643c7d9849b242|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_ef5badfdf820c54672609c5ce31d40d3}%

.. code-block:: php

    <?php

    $this->view->setVars(array('products' => $products));





%{Phalcon_Mvc_View_e4e3f6b4d5a4a191a860ca66a6e6bd4a|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_a7c2d95e5c08800de66c815eb24b1bf7}%

.. code-block:: php

    <?php

    $this->view->setVar('products', $products);





%{Phalcon_Mvc_View_f280bc12a8b7dd226062887c75b3356c}%

%{Phalcon_Mvc_View_556f0eb6745826c2bec05d03f3cd6b8f}%

%{Phalcon_Mvc_View_0ef6adc104f7ff5d55626c20e3d39118}%

%{Phalcon_Mvc_View_75aff7a4f047fd34c7075c73592d353d}%

%{Phalcon_Mvc_View_63721017129ab4ca298b33caa1721e91}%

%{Phalcon_Mvc_View_fa6b227a55fd2007bc545c83dd68d7b1}%

%{Phalcon_Mvc_View_7653c5621328184a0ef6712f5be7b5b7}%

%{Phalcon_Mvc_View_b44431f2e894dba9424567941717a2ff}%

%{Phalcon_Mvc_View_d88c3713331c25b77ed605f801609a8c}%

%{Phalcon_Mvc_View_44c94746ea72946316665a443a482651}%

%{Phalcon_Mvc_View_1e604d3dfebe51d8bb2b49f0da5b659c|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_2a393912ab0f0f6c1aed6b8c428dd954}%

%{Phalcon_Mvc_View_849ea7f36c775b798e83e079025de0f1}%

%{Phalcon_Mvc_View_c5fad474a79599299afb29ee02f12e73}%

%{Phalcon_Mvc_View_ac0662ca4ccb1c97d8baed5befa65b4b}%

%{Phalcon_Mvc_View_e454799fa9b9cdd237ccb21826db876e}%

%{Phalcon_Mvc_View_a1f2c96cd94343dc2872dbf7a4d1cf6c|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_dc053761cc0f4d07a7c906896ef9ab67}%

.. code-block:: php

    <?php

    $this->view->registerEngines(array(
      ".phtml" => "Phalcon\Mvc\View\Engine\Php",
      ".volt" => "Phalcon\Mvc\View\Engine\Volt",
      ".mhtml" => "MyCustomEngine"
    ));





%{Phalcon_Mvc_View_46261288d993a8902798a46d632bc97a}%

%{Phalcon_Mvc_View_40e808e88fcdca48db5c8457d17d5d67}%

%{Phalcon_Mvc_View_578d7968f8d16cfc43580d3813b065ec}%

%{Phalcon_Mvc_View_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Mvc_View_8589d33bfb10e55638e6fbb72dea82f9|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_6eaf7957186c7dac375b55e792e069fd}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_View_ae3151340a5be99b85680be73edcfea8%}
     $view->start()->render('posts', 'recent')->finish();





%{Phalcon_Mvc_View_0e375227fd673844e6c876ae517d302c|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_d474936d09c42a34f70f44f7a9b50a4d}%

.. code-block:: php

    <?php

     class ProductsController extends Phalcon\Mvc\Controller
     {
    
        public function saveAction()
        {
    
             //{%Phalcon_Mvc_View_4bb1314384a4b571d316b291a8deb8d3%}
    
             //{%Phalcon_Mvc_View_d51787c066501a3e8e1a8c261d256b8d%}
             $this->view->pick("products/list");
        }
     }





%{Phalcon_Mvc_View_e459588598397e8673f20018e3896417}%

%{Phalcon_Mvc_View_5fb87964fde637a4d5a9021340c846f3}%

.. code-block:: php

    <?php

     	//{%Phalcon_Mvc_View_03ae2ce0d1403555685f00c92ada0a28%}
     	$this->partial('shared/footer');

.. code-block:: php

    <?php

     	//{%Phalcon_Mvc_View_525a51cf377d9a86611411ec4f7b0093%}
     	$this->partial('shared/footer', array('content' => $html));





%{Phalcon_Mvc_View_1e9fb865328dae0458002e1aff82ae16}%

%{Phalcon_Mvc_View_b62a38843924316ce41fd5918283902f}%

.. code-block:: php

    <?php

     	$template = $this->view->getRender('products', 'show', array('products' => $products));





%{Phalcon_Mvc_View_15ae08bfc8bb826426c419c2e7c0fc68|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_b3eee5f1b69992520c3d7f26ee6623a2}%

%{Phalcon_Mvc_View_90acf57c85190fe4d253a140e1dcca8b|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_View_ef7d466da826b95a531646e34c64ca81}%

%{Phalcon_Mvc_View_7d6dbf5b27064dab4f0ca0ce5021a43f}%

%{Phalcon_Mvc_View_0ad24f5c47bf8324d08c40321d2e6c8c}%

%{Phalcon_Mvc_View_09b5c9851ac944b212045804108afe45|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_View_bd3e82819d1fdd7d992782a4603bd0e9}%

%{Phalcon_Mvc_View_4e2059b4e3ca2893e39dc064233a5b88|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_d4a1fab2d9e9cf2fb183dd5a1da9d239}%

.. code-block:: php

    <?php

      $this->view->cache(array('key' => 'my-key', 'lifetime' => 86400));





%{Phalcon_Mvc_View_4818072e842c63505de67eeb65d1feea|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_e96a422159b67ba5238340257fee8bfb}%

.. code-block:: php

    <?php

    $this->view->setContent("<h1>hello</h1>");





%{Phalcon_Mvc_View_098b283de06b3d606bc941e79200d4a9}%

%{Phalcon_Mvc_View_76a1afcd13a1a6009c8dd3d8a0d942ba}%

%{Phalcon_Mvc_View_c4504fe9fbe9dfb93bbd3cbf4ee751e0}%

%{Phalcon_Mvc_View_38b425be8f2830ec0b4d97a4c7878999}%

%{Phalcon_Mvc_View_985f6cc2606916f165e98ca5d2711aa1|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_d01c14a836b5d7b92f12ec6f54bdcf2d}%

%{Phalcon_Mvc_View_ddc16f24eb2b5003b298ef3b54da20ae|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_72686f6618da8e40ad717e9372e090a4}%

%{Phalcon_Mvc_View_204d12278902ad3661bdd50d3ba6ce34}%

%{Phalcon_Mvc_View_95d458a6583c7ca32c5af957e5a1dd02}%

%{Phalcon_Mvc_View_0e03074bb6c96e79b3ec3cb36df4bf5f|:doc:`Phalcon\\Mvc\\View <Phalcon_Mvc_View>`}%

%{Phalcon_Mvc_View_32ea68b990143515d2fc6abc91a361c7}%

%{Phalcon_Mvc_View_75b08ba9d8a63caef75a55aba58acf44}%

%{Phalcon_Mvc_View_cc332ac586888c98722310f153c980e7}%

.. code-block:: php

    <?php

    $this->view->products = $products;





%{Phalcon_Mvc_View_f66fc80d477b58d5bc2da417547dd454}%

%{Phalcon_Mvc_View_0f4889ec0422f0973699057ce29d773e}%

.. code-block:: php

    <?php

    echo $this->view->products;





%{Phalcon_Mvc_View_e559656e6c4d8b1c50eacd0266af7d09}%

%{Phalcon_Mvc_View_c36604426dd6358cfb5ec06fbc054467}%

.. code-block:: php

    <?php

    isset($this->view->products)





%{Phalcon_Mvc_View_d98a2143a4e9b08520a7df0009a78266|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_View_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Mvc_View_3e7e4ddacc724c78120150572bdab704|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_View_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_View_5e6f785f2df62ae1a2a672663fb6b2e4|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_View_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Mvc_View_8d859c2f0efce8383f08ae517e34a4af|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_View_fddf8905978e673c97a301a70bb79d53}%

