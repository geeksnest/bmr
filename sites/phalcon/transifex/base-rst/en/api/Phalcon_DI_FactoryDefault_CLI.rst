%{Phalcon_DI_FactoryDefault_CLI_7eb57436809c197c790be2a563f57f4b}%
==========================================

%{Phalcon_DI_FactoryDefault_CLI_8ad34d6e62fb4ea22d4922f529ff43d6|:doc:`Phalcon\\DI\\FactoryDefault <Phalcon_DI_FactoryDefault>`}%

%{Phalcon_DI_FactoryDefault_CLI_db204ff003282afa0ef9260aa7ce4edd|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_2a259614a2a9a824c9174988514646e3}%

%{Phalcon_DI_FactoryDefault_CLI_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_DI_FactoryDefault_CLI_a5d0ee958da5dcfe392fa79afb9d9a90}%

%{Phalcon_DI_FactoryDefault_CLI_21870649cf423c67af1200420a3574ef}%

%{Phalcon_DI_FactoryDefault_CLI_6c37d2c204beac01eb9662914195788b|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_ce01cc397cce56ba4bef7cefc5250d1d}%

%{Phalcon_DI_FactoryDefault_CLI_05e34023e94b56c9a2a231d03f5f681d}%

%{Phalcon_DI_FactoryDefault_CLI_e4e5dbbed56b2951d0b08e1e2490eac1}%

%{Phalcon_DI_FactoryDefault_CLI_d95d9d527dac726b2d7580e179baf043}%

%{Phalcon_DI_FactoryDefault_CLI_0835f3f89ef8c3109362d3c6ae9c7a27}%

%{Phalcon_DI_FactoryDefault_CLI_52a4c767b69e8e6de34773c95920a302|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_d32192e4aa1620230493efd07507a036}%

%{Phalcon_DI_FactoryDefault_CLI_b01c92a019625beeac918ec734a5a8ff|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_4168b228435201e1c21c5f9abb8f3f28}%

%{Phalcon_DI_FactoryDefault_CLI_b7f5061fc1f3951529265cc67118be94}%

%{Phalcon_DI_FactoryDefault_CLI_95d334afb740c3f0f3d417fb410e1dc2}%

%{Phalcon_DI_FactoryDefault_CLI_03cb5bdbb0497c3bd29d62166203985b}%

%{Phalcon_DI_FactoryDefault_CLI_19182972f82e522e5efba84a14b57bc5}%

%{Phalcon_DI_FactoryDefault_CLI_3791112b2b8f40e088f5cd9bd36055b9}%

%{Phalcon_DI_FactoryDefault_CLI_9898af756c69a399d3187da346fe25ee}%

%{Phalcon_DI_FactoryDefault_CLI_d935f776bce8f9180e68d832486a3030}%

%{Phalcon_DI_FactoryDefault_CLI_0bfccbc8a46df8525956efc2b3759cc1}%

%{Phalcon_DI_FactoryDefault_CLI_fb5071fa5112be19aeb299fea339fade|:doc:`Phalcon\\DI\\Service <Phalcon_DI_Service>`}%

%{Phalcon_DI_FactoryDefault_CLI_3f570b0d3f0e849a87d6ae771abf7ae5}%

%{Phalcon_DI_FactoryDefault_CLI_5b10e2ac4c20f2ee83fc876f07359b2a|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_c5307d09bac945bb6e36aeb19eaf0395}%

%{Phalcon_DI_FactoryDefault_CLI_83a127a13f0c1f82e0bea3950bcc9362|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_64d8e5a7725ea3061f1e1fb50e58e7a2}%

%{Phalcon_DI_FactoryDefault_CLI_416e08f12a01090574055e2cf85f057c}%

%{Phalcon_DI_FactoryDefault_CLI_76ca6388e9a3681f681fb632077226b6}%

%{Phalcon_DI_FactoryDefault_CLI_2ec39f142c20c0f6576bf470d72e18cd|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_e037e940a2f8bfb2b292ded1cf926c20}%

%{Phalcon_DI_FactoryDefault_CLI_fdb5d7f4c9b6e678131debc61a7dce2f|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_FactoryDefault_CLI_194ecde9db15b716a22cee811483fddb}%

%{Phalcon_DI_FactoryDefault_CLI_4b76277fdd096d4cbfb4a49787d80d07}%

%{Phalcon_DI_FactoryDefault_CLI_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_DI_FactoryDefault_CLI_7bf0b3ef8ef370fd757d45604d7fe134}%

%{Phalcon_DI_FactoryDefault_CLI_936137f836bd590d4dd24d4ca528c18b}%

%{Phalcon_DI_FactoryDefault_CLI_799496ac0d8000ac393b066f968f6435}%

%{Phalcon_DI_FactoryDefault_CLI_e039ec54f14173293180bfd0b9ff56f9}%

.. code-block:: php

    <?php

    $di['request'] = new Phalcon\Http\Request();





%{Phalcon_DI_FactoryDefault_CLI_33f6c80a4eba48aa8b5ca8815e120615}%

%{Phalcon_DI_FactoryDefault_CLI_3e13ec2ea018ad42c6f00583882ed251}%

.. code-block:: php

    <?php

    var_dump($di['request']);





%{Phalcon_DI_FactoryDefault_CLI_a1aed4441619376fd04ccadc56dc0ebe}%

%{Phalcon_DI_FactoryDefault_CLI_0439e217cf83548ea2dd4a0754c91190}%

%{Phalcon_DI_FactoryDefault_CLI_6d4516ae2c4692295023c9176e902c78}%

%{Phalcon_DI_FactoryDefault_CLI_82c598b86b87b6fac0fe51c6b80a80d0}%

%{Phalcon_DI_FactoryDefault_CLI_34b10bbdaabb3b33cec89665a821e640}%

%{Phalcon_DI_FactoryDefault_CLI_68cf0a3bff1e41b907cf955f570c5249}%

