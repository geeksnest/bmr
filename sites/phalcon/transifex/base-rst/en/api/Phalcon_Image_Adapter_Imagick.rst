%{Phalcon_Image_Adapter_Imagick_d09294d8e9fc49ab3e2c52194594186d}%
==========================================

%{Phalcon_Image_Adapter_Imagick_827b9d36a7ffc35d19a44231e8dba93e|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_e72c6bad6f1ae6f44b27956f36582611|:doc:`Phalcon\\Image\\AdapterInterface <Phalcon_Image_AdapterInterface>`}%

%{Phalcon_Image_Adapter_Imagick_b003776dbe8411732f79ad5c2ecfef16}%

.. code-block:: php

    <?php

    $image = new Phalcon\Image\Adapter\Imagick("upload/test.jpg");
    $image->resize(200, 200)->rotate(90)->crop(100, 100);
    if ($image->save()) {
    	echo 'success';
    }




%{Phalcon_Image_Adapter_Imagick_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Image_Adapter_Imagick_96c5aee990f412c878bde2e76e25ac2e}%

%{Phalcon_Image_Adapter_Imagick_8693744160eb0ff0b39723e0577cc811}%

%{Phalcon_Image_Adapter_Imagick_98865f43d984e2ab1cfcd1b2af8c518b}%

%{Phalcon_Image_Adapter_Imagick_961ba1cc7024b317b94a94ba82659667}%

%{Phalcon_Image_Adapter_Imagick_be1aed0238ed7d9938fa2a7937a96333}%

%{Phalcon_Image_Adapter_Imagick_b1e6154b84a8263776338778ddef5be3}%

%{Phalcon_Image_Adapter_Imagick_ca703fca018dda97b385eef9969a87b4}%

%{Phalcon_Image_Adapter_Imagick_d15447ed4be4349529b874f73e9bf140}%

%{Phalcon_Image_Adapter_Imagick_3f3ce721706657548a5d63dfb0a9a041}%

%{Phalcon_Image_Adapter_Imagick_83eb557976ff8cf6228af1e60c9f52b3}%

%{Phalcon_Image_Adapter_Imagick_85f4903cb0fdc21b86de419bd48ab4c3}%

%{Phalcon_Image_Adapter_Imagick_06dd51fb59c23ac64b3efada063ad69e}%

%{Phalcon_Image_Adapter_Imagick_18fc7b3d7b78a0cbb06204f1f66a8c4a}%

%{Phalcon_Image_Adapter_Imagick_b1e2fe09f0b5b865d430e1f811eab116}%

%{Phalcon_Image_Adapter_Imagick_3f455dced414530fdf484fe7ea24c171}%

%{Phalcon_Image_Adapter_Imagick_0cb80bd9acdd5ff7f5e6faec72350a9a}%

%{Phalcon_Image_Adapter_Imagick_1a2bdd80e5012612942ba9599c089d6b}%

%{Phalcon_Image_Adapter_Imagick_a25d544f77e5344ec3b91e4aa7948e7a}%

%{Phalcon_Image_Adapter_Imagick_a4b5a103de3b91037ca44a0f600b62f2|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_ffcd1ad9b19d1fa5272580412bdf8fa0}%

%{Phalcon_Image_Adapter_Imagick_cbcd3a8000f1d1386b51037e59eda446}%

%{Phalcon_Image_Adapter_Imagick_ed05f63abfe118b94d320c1a3bbc7676}%

%{Phalcon_Image_Adapter_Imagick_2cc2dfe458e75fff4bfd73bd9c8a2846}%

%{Phalcon_Image_Adapter_Imagick_04227e374bc19c99406bbd50ecf0d959}%

%{Phalcon_Image_Adapter_Imagick_7121c8d9ac72d4bb15e1186655893b7c}%

%{Phalcon_Image_Adapter_Imagick_2ce8b20f5a4fb533cc80e5a694e6903b}%

%{Phalcon_Image_Adapter_Imagick_1393084579d6cc1feebe1f163f75799e}%

%{Phalcon_Image_Adapter_Imagick_2110446b65e9c3e878dabc1d8df0d8b0}%

%{Phalcon_Image_Adapter_Imagick_2f6b2edc8aac3cb2483c9858d777ff27}%

%{Phalcon_Image_Adapter_Imagick_ecc5022fba3216779e50af03d420ad64}%

%{Phalcon_Image_Adapter_Imagick_7ba1c89a23f9ceb412d09cc06c162ee5}%

%{Phalcon_Image_Adapter_Imagick_b7d39512a0343a47fee85f9ca4d252c7}%

%{Phalcon_Image_Adapter_Imagick_02ec4c7e6df44aea69b3e74bde30f498}%

%{Phalcon_Image_Adapter_Imagick_88185bb94e8a70d4e825434e875cc02e}%

%{Phalcon_Image_Adapter_Imagick_4efdfc6db0248d6e3f98218f90fe9b22}%

%{Phalcon_Image_Adapter_Imagick_43c05e6b55ed1f8c0cf0ce463f335459}%

%{Phalcon_Image_Adapter_Imagick_a3881f3c6a737ab9ad65d1934c947bf3}%

%{Phalcon_Image_Adapter_Imagick_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_Imagick_2f7c5efce9c1c27c4f8d2eed6d3b81f8}%

%{Phalcon_Image_Adapter_Imagick_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_Imagick_c150431f2ed7af3f7de51d62960fb26e}%

%{Phalcon_Image_Adapter_Imagick_bc3f4afc28cf64bcf4a2c1533342feda}%

%{Phalcon_Image_Adapter_Imagick_527d77dca48cad04fc2679ccfbb3f0d7}%

%{Phalcon_Image_Adapter_Imagick_9e9720e38ca04be3162ee675854b12ae}%

%{Phalcon_Image_Adapter_Imagick_833202cd0cb04ed347f7dd744e7d1715}%

%{Phalcon_Image_Adapter_Imagick_feae7c99ef013b87b02dd6315f82b081}%

%{Phalcon_Image_Adapter_Imagick_c1afc61e44a88e9bd2710756b6394349}%

%{Phalcon_Image_Adapter_Imagick_01e98089aa2442ad0260a82f61c9e272}%

%{Phalcon_Image_Adapter_Imagick_d843005daeb3a16d3a9bc3b8ce50784a}%

%{Phalcon_Image_Adapter_Imagick_bcea4f38bd2aa3cedc96001ece4b6084}%

%{Phalcon_Image_Adapter_Imagick_75c5e888533b4b8293f001699b64b174}%

%{Phalcon_Image_Adapter_Imagick_0b8aef9d269b4e47c6bb5755aa2fdd31}%

%{Phalcon_Image_Adapter_Imagick_2c19d0bb337bc491ce85ca6e005971c1|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_67f6473560ed054927ccd8bd4c6022b1}%

%{Phalcon_Image_Adapter_Imagick_2c31ddffe77fd25f8645cc624ac2c948|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_d15447ed4be4349529b874f73e9bf140}%

%{Phalcon_Image_Adapter_Imagick_ddfff48ca407b1b58ceea3e7d852b41d|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_a35195cd99d7d13daf538c3f3594fd71}%

%{Phalcon_Image_Adapter_Imagick_2c90849344e7329a9eefb7446c9cacda|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_e00e71ca6ee23ce529d38d2208bafdf7}%

%{Phalcon_Image_Adapter_Imagick_f685c4757fa19c362ca9e24b3323a663|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_ee6db05cd386b5aa8caa5329b5941aa4}%

%{Phalcon_Image_Adapter_Imagick_61618f2f189fa48ac89407375cb22dc3|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_35d6c150350a4435617e18fb30030f96}%

%{Phalcon_Image_Adapter_Imagick_243659d85dc6375f6930059f116f153e|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_f7fcebc47d482a284953ee72539bb098}%

%{Phalcon_Image_Adapter_Imagick_f331b41fb40cf99ebc1a9143ff750eb6|:doc:`Phalcon\\Image\\AdapterInterface <Phalcon_Image_AdapterInterface>`}%

%{Phalcon_Image_Adapter_Imagick_3a2d417c9d89eade0dc6f0a9422ad499}%

%{Phalcon_Image_Adapter_Imagick_bd1ab141dd7c08cad729b7b345ca3b91|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_816bcf1eed6f3beeb043f8cd89cc03fc}%

%{Phalcon_Image_Adapter_Imagick_1b47a22bce0d18e13ea43932237870f6|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_04227e374bc19c99406bbd50ecf0d959}%

%{Phalcon_Image_Adapter_Imagick_4471f3c670202a041d9548a998f4e7b9|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_dd872c7e134e6531c1d94dfe421b9dbc}%

%{Phalcon_Image_Adapter_Imagick_3b44230599501c97fda1057574f40bb5|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_2110446b65e9c3e878dabc1d8df0d8b0}%

%{Phalcon_Image_Adapter_Imagick_66c550bb213c2c36ded0543ab52546de|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_ecc5022fba3216779e50af03d420ad64}%

%{Phalcon_Image_Adapter_Imagick_c3925a3f2ce55c407e5cfd1e2b1206d6}%

%{Phalcon_Image_Adapter_Imagick_cba770832f99922c6ccd2b64fd158d65}%

%{Phalcon_Image_Adapter_Imagick_11243918020bd28bbe05020f5148ed2c|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_Imagick_d82d98067ee7bc0b11560c5b87cb52ee}%

