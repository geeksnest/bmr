%{Phalcon_Cache_Frontend_Json_a2a1323479d378da6be03bf58627fb08}%
========================================

%{Phalcon_Cache_Frontend_Json_162bdfd7b52f9a8bbf25b7cb2701bc34|:doc:`Phalcon\\Cache\\Frontend\\Data <Phalcon_Cache_Frontend_Data>`}%

%{Phalcon_Cache_Frontend_Json_d4caea1dc5a1b9f969431a18993f6133|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Frontend_Json_38a41829308d77d09ae9306b026ae81b}%

.. code-block:: php

    <?php

     // {%Phalcon_Cache_Frontend_Json_7ff6a1b61f9a0143b007a169db1f146c%}
     $frontCache = new Phalcon\Cache\Frontend\Json(array(
        "lifetime" => 172800
     ));
    
     //{%Phalcon_Cache_Frontend_Json_0748df875c42080145b360c2eb6a826b%}
     $cache = new Phalcon\Cache\Backend\Memcache($frontCache, array(
    	'host' => 'localhost',
    	'port' => 11211,
      	'persistent' => false
     ));
    
     //{%Phalcon_Cache_Frontend_Json_f3197c319b2e1bb4ba59291a6ad75fa9%}
     $cache->save('my-data', array(1, 2, 3, 4, 5));
    
     //{%Phalcon_Cache_Frontend_Json_45b6351e81b39eda563b06fcc2f2b1ff%}
     $data = $cache->get('my-data');




%{Phalcon_Cache_Frontend_Json_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Frontend_Json_346147a9407546082e932b7555d6ae42}%

%{Phalcon_Cache_Frontend_Json_c13a0dfffc22f2c1ccc169fcc2155061}%

%{Phalcon_Cache_Frontend_Json_bf8d76d841c9e0ff5bc2145390806e3e}%

%{Phalcon_Cache_Frontend_Json_c63fe6767d99141ee6bb05bfce314297}%

%{Phalcon_Cache_Frontend_Json_08c177613301040bd72546ff68086470}%

%{Phalcon_Cache_Frontend_Json_1ae81dbbfeea219e1d883f5ae0052e6a}%

%{Phalcon_Cache_Frontend_Json_cb420b927c72015a1baed352fde19602}%

%{Phalcon_Cache_Frontend_Json_790ba726a84e8cc7d11c01ae6aeda21b}%

%{Phalcon_Cache_Frontend_Json_fb4ea0b507a29eca01a0f21e824f87cb}%

%{Phalcon_Cache_Frontend_Json_1df518f9eabe9f817151b63df9f51b70}%

%{Phalcon_Cache_Frontend_Json_722c2e1e16bfe6f0d096aae0c9166480}%

%{Phalcon_Cache_Frontend_Json_1ff5bfe3b4c822dd286e8ce81aac1b8b}%

%{Phalcon_Cache_Frontend_Json_b145d17dcbbadf9356b39116f7244b29}%

%{Phalcon_Cache_Frontend_Json_e3e99c0a1d8cbb1a36ae911dcb475f20}%

%{Phalcon_Cache_Frontend_Json_d9632a3c1f7822b67ab24ed1bf397249}%

%{Phalcon_Cache_Frontend_Json_3ff217b55d51515949922d3d8f3004eb}%

