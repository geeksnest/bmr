%{Phalcon_Http_Response_3bfbbf7def31320dd9bdc8f2e0943564}%
=================================

%{Phalcon_Http_Response_569c1c8588c12124de939534a702bef1|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Http_Response_de15934a38c96f9a62e3690ddbe70cd3}%

.. code-block:: php

    <?php

    $response = new Phalcon\Http\Response();
    $response->setStatusCode(200, "OK");
    $response->setContent("<html><body>Hello</body></html>");
    $response->send();




%{Phalcon_Http_Response_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Http_Response_cc45268936cc86ca5e667eb056492196}%

%{Phalcon_Http_Response_6a1478c9cfc9d61dd5380947aee25abb}%

%{Phalcon_Http_Response_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Response_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Http_Response_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Response_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Http_Response_d117f3a022dd263a4e430b13b5da72b8|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_e605a5b8bbd78718e5d50478fbaf931a}%

.. code-block:: php

    <?php

    $response->setStatusCode(404, "Not Found");





%{Phalcon_Http_Response_8bb5c8a46800f394a38dbd3ffc25629f|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`|:doc:`Phalcon\\Http\\Response\\HeadersInterface <Phalcon_Http_Response_HeadersInterface>`}%

%{Phalcon_Http_Response_265ec5a6a6900aa78e443a46684f366b}%

%{Phalcon_Http_Response_d10655680c09d9fda850074f4cf06232|:doc:`Phalcon\\Http\\Response\\HeadersInterface <Phalcon_Http_Response_HeadersInterface>`}%

%{Phalcon_Http_Response_f237a27f2a14bf894e12d1e11fe3be47}%

%{Phalcon_Http_Response_6b953fc7b6e8cde78b3ccf3f367c2109|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`|:doc:`Phalcon\\Http\\Response\\CookiesInterface <Phalcon_Http_Response_CookiesInterface>`}%

%{Phalcon_Http_Response_610d3eb12e1f295f81ff2c11276fb449}%

%{Phalcon_Http_Response_91d7daed2aae1a25ff0d3eafed1ffc11|:doc:`Phalcon\\Http\\Response\\CookiesInterface <Phalcon_Http_Response_CookiesInterface>`}%

%{Phalcon_Http_Response_92de31b53ada4c1fa16564adf57862dd}%

%{Phalcon_Http_Response_676c580bc094c5740a5f9658c1f137dc|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_32b4554d417b5699d7f829059e3c32c2}%

.. code-block:: php

    <?php

    $response->setHeader("Content-Type", "text/plain");





%{Phalcon_Http_Response_e4fbbc9077ee92340f82ba4eb061e39f|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_3ae24acb15a532684ed2af3715a37595}%

.. code-block:: php

    <?php

    $response->setRawHeader("HTTP/1.1 404 Not Found");





%{Phalcon_Http_Response_0242ba428721f844d74897c9f7fdfed4|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_72ea85a5beb6771e50aef50ff418ebf2}%

%{Phalcon_Http_Response_86b2552124634e825f5061468b3552ea}%

%{Phalcon_Http_Response_82670c328e2b6f5c87b8c7096dbfdbf0}%

.. code-block:: php

    <?php

    $this->response->setExpires(new DateTime());





%{Phalcon_Http_Response_2f2ae92c1c6c4b9db29e9e203603c012|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_b32022b0ac7fb7906174677f0bf12157}%

%{Phalcon_Http_Response_127aa3e5e6dfb923989dadea3f726e04|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_cfdde7a4a17260e832b63e7e1ba42060}%

.. code-block:: php

    <?php

    $response->setContentType('application/pdf');
    $response->setContentType('text/plain', 'UTF-8');





%{Phalcon_Http_Response_b228902a0a70ec1e180a02c36f706671}%

%{Phalcon_Http_Response_7500bd7bd2eebe4a18e3cea10a645772}%

.. code-block:: php

    <?php

    $response->setEtag(md5(time()));





%{Phalcon_Http_Response_43f87356c074ecb4a55c3d30e6100b2a|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_21b7629055d840701aa4ae3d9608fc62}%

.. code-block:: php

    <?php

      //{%Phalcon_Http_Response_af6a8e5fb30355d21f0e168ec6dc84ce%}
    $response->redirect("posts/index");
    $response->redirect("http://en.wikipedia.org", true);
    $response->redirect("http://www.example.com/new-location", true, 301);
    
    //{%Phalcon_Http_Response_8c95d09124eb906dfc79172a058a25cb%}
    $response->redirect(array(
    	"for" => "index-lang",
    	"lang" => "jp",
    	"controller" => "index"
    ), null, 307);





%{Phalcon_Http_Response_08ed9cd5cb8956bf7d9a4bb10cb31ed9|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_01c20a8037498f88a5de03a34785b48e}%

.. code-block:: php

    <?php

    $response->setContent("<h1>Hello!</h1>");





%{Phalcon_Http_Response_602c168872c09c1a0499a34de27f5a1f|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_32f66805172ec27aca9184ce2ce505a7}%

.. code-block:: php

    <?php

    $response->setJsonContent(array("status" => "OK"));
    $response->setJsonContent(array("status" => "OK"), JSON_NUMERIC_CHECK);

*




%{Phalcon_Http_Response_20cf8a01e49ec0cdfc5d5d3c74dc26f9|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_f3d56b7967c79a14a998c125af31df4b}%

%{Phalcon_Http_Response_098b283de06b3d606bc941e79200d4a9}%

%{Phalcon_Http_Response_747a092609cbec75f48b5fef50713c3b}%

%{Phalcon_Http_Response_a9a3b86421b8f408cac8eb0dd9f531c8}%

%{Phalcon_Http_Response_aba27d19884a89483de28bad07072368}%

%{Phalcon_Http_Response_63d1525bc1f58f7fd36b7fc8db94cd9a|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_444b1912c07d9862a0b096787d55dd15}%

%{Phalcon_Http_Response_46d8e46fcf05ff014e5957fb364be003|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_3c08474bb26c9adc1688023efef41bb0}%

%{Phalcon_Http_Response_508238e638c9509d3aee26c95d8122d1|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_Response_2367c18633dbaf3106d8e843a569fbe1}%

%{Phalcon_Http_Response_73b55891ebecf6b92c663a2681dc3ae3}%

%{Phalcon_Http_Response_dd29378305e2f0a39ddbcaac63aec8b8}%

