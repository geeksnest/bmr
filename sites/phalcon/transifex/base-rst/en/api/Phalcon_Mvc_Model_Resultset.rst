%{Phalcon_Mvc_Model_Resultset_6993966cc8b04d14fc6823285ea68f76}%
=================================================

%{Phalcon_Mvc_Model_Resultset_e2ef448e6ffcaf9fb372b6c83bd91fb4|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_Resultset_a3a671840680344838251fcf2ffb085d}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_Resultset_a5e4fa72310833420501e5944496d470%}
     $robots = Robots::find(array("type='virtual'", "order" => "name"));
     foreach ($robots as $robot) {
      echo $robot->name, "\n";
     }
    
     //{%Phalcon_Mvc_Model_Resultset_8b7c1365ead38f0e263c250fba556159%}
     $robots = Robots::find(array("type='virtual'", "order" => "name"));
     $robots->rewind();
     while ($robots->valid()) {
      $robot = $robots->current();
      echo $robot->name, "\n";
      $robots->next();
     }




%{Phalcon_Mvc_Model_Resultset_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Model_Resultset_b110d762ef16e0a26e39b1749392c7cc}%

%{Phalcon_Mvc_Model_Resultset_1334405c9960d60af4d1391cd1aa35ff}%

%{Phalcon_Mvc_Model_Resultset_55a5e437ff48bc859a9b346789b793a8}%

%{Phalcon_Mvc_Model_Resultset_3d3f9eff296dfe75e7a9d80248c7acca}%

%{Phalcon_Mvc_Model_Resultset_d172c6ce9ee5f9392cae87c67ec7d8d8}%

%{Phalcon_Mvc_Model_Resultset_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Resultset_2b503cefe62dd539e45818e4fd0f9833}%

%{Phalcon_Mvc_Model_Resultset_0637e194ca8055b68d1862383d6335f8}%

%{Phalcon_Mvc_Model_Resultset_78ee93ae348293a9c99ee9d4078b1577}%

%{Phalcon_Mvc_Model_Resultset_587a0790bfb8c682992164d63484cdc2}%

%{Phalcon_Mvc_Model_Resultset_d07d7e709333e5a70beedd81cc47a746}%

%{Phalcon_Mvc_Model_Resultset_da276b8f032e1bb8ed0a5ab2d42d19aa}%

%{Phalcon_Mvc_Model_Resultset_383e5203a4d17e7fb271a647ff82abe0}%

%{Phalcon_Mvc_Model_Resultset_59caf172c77cbe7767ef9b01f0f59f14}%

%{Phalcon_Mvc_Model_Resultset_e3d7fb712da63af712a3f3fe13d5d75b}%

%{Phalcon_Mvc_Model_Resultset_9c113a5079ff20ad0bafc23e1b5872b3}%

%{Phalcon_Mvc_Model_Resultset_87480220bbdc87f5b55fced97c5105c6}%

%{Phalcon_Mvc_Model_Resultset_d4a3553e435c7e74a5b00878654a422c}%

%{Phalcon_Mvc_Model_Resultset_0932b21a70a56301a25739a59052b0ee|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_d349b7c4fed51672ae8a7418498107c1}%

%{Phalcon_Mvc_Model_Resultset_9176a7e3fbe6d6339fc2d05f4317ca92|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_ce3ddc01aaa610f5e49a87bc872089e7}%

%{Phalcon_Mvc_Model_Resultset_3644e01a19440baa19219140adc68825}%

%{Phalcon_Mvc_Model_Resultset_ce3ddc01aaa610f5e49a87bc872089e7}%

%{Phalcon_Mvc_Model_Resultset_476f2497c851eae8a78f73032ad317bb}%

%{Phalcon_Mvc_Model_Resultset_4b657a29293e7e5c2a6fac3729dc1806}%

%{Phalcon_Mvc_Model_Resultset_9b3f70d83a5a51330fd01b03a934deed|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_f49ace34534dc42ef31d0885e4948b27}%

%{Phalcon_Mvc_Model_Resultset_e4d0754f446e96d3428d79374b4cceca|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_f47a3d422216ccaf8480e2d12484e793}%

%{Phalcon_Mvc_Model_Resultset_0f37fad8e6925e468cb8413961310c70|:doc:`Phalcon\\Mvc\\Model\\Resultset <Phalcon_Mvc_Model_Resultset>`}%

%{Phalcon_Mvc_Model_Resultset_0cc74caa22d6c02f693257654583a5c1}%

%{Phalcon_Mvc_Model_Resultset_87f781916e0bd849d776b73a276514d3}%

%{Phalcon_Mvc_Model_Resultset_a60dae6e6a9d3048832a81aaa7ad7aca}%

%{Phalcon_Mvc_Model_Resultset_879cd2bc81138388d2b5e36e6cec569f|:doc:`Phalcon\\Mvc\\Model\\Resultset <Phalcon_Mvc_Model_Resultset>`}%

%{Phalcon_Mvc_Model_Resultset_6bb86d55a16dd81bf45766f15a1a6e94}%

%{Phalcon_Mvc_Model_Resultset_4824034e762a207f985c22b4ae879c26}%

%{Phalcon_Mvc_Model_Resultset_d3414d206426516d2132fd030410f370}%

%{Phalcon_Mvc_Model_Resultset_09b5c9851ac944b212045804108afe45|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_Model_Resultset_10985f4916f50d49c95bcef66585df0f}%

%{Phalcon_Mvc_Model_Resultset_529a64b2f5676fcb86f442fc9b51893f|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_7d39aa784396da33b1f190e2389cab4a}%

%{Phalcon_Mvc_Model_Resultset_0275cb947f8840a688e0d40528dc8dcd|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Model_Resultset_f3bb945cd58ec7b508db2654f4133ed6}%

%{Phalcon_Mvc_Model_Resultset_72a36325cd19fadd5faf8bcbdd7c78e7}%

%{Phalcon_Mvc_Model_Resultset_29646fd89480ebac97d734c5e2bfda09}%

%{Phalcon_Mvc_Model_Resultset_5a5cf011ea18af49393ce8fe14ececf7|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Resultset_e8845c00d77f057be2d185873b604218}%

.. code-block:: php

    <?php

     $filtered = $robots->filter(function($robot){
    	if ($robot->id < 3) {
    		return $robot;
    	}
    });





%{Phalcon_Mvc_Model_Resultset_4ef43120747d11f7a527fafc4659c304}%

%{Phalcon_Mvc_Model_Resultset_532c5dbe59c87799c9526761d084ca77}%

%{Phalcon_Mvc_Model_Resultset_090651966c33472383df868b32df1f5b}%

%{Phalcon_Mvc_Model_Resultset_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Mvc_Model_Resultset_3efd0a4dc9fdb8b8b74573175ccc6e24}%

%{Phalcon_Mvc_Model_Resultset_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Mvc_Model_Resultset_cb5c0dddb9b2ce2a00c182f318a3b280}%

%{Phalcon_Mvc_Model_Resultset_68cf0a3bff1e41b907cf955f570c5249}%

