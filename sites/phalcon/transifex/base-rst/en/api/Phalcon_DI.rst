%{Phalcon_DI_729b71164746960be73ff9e591aea35e}%
=====================

%{Phalcon_DI_db204ff003282afa0ef9260aa7ce4edd|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_adb7517767b1d731c0fb091c7716601c}%

.. code-block:: php

    <?php

     $di = new Phalcon\DI();
    
     //{%Phalcon_DI_f468f1d6513d7a485e5df3900c006149%}
     $di->set('request', 'Phalcon\Http\Request', true);
    
     //{%Phalcon_DI_2f1fffb20b38514cec7ec9556bcc1901%}
     $di->set('request', function(){
      return new Phalcon\Http\Request();
     }, true);
    
     $request = $di->getRequest();




%{Phalcon_DI_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_DI_a5d0ee958da5dcfe392fa79afb9d9a90}%

%{Phalcon_DI_6abcb1dc3525f2c112cdab3b5be5442d}%

%{Phalcon_DI_56a52c6f6fa627436521072072c7cc99|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_ce01cc397cce56ba4bef7cefc5250d1d}%

%{Phalcon_DI_1489832a9630a21b36eccde0c9624df9}%

%{Phalcon_DI_e4e5dbbed56b2951d0b08e1e2490eac1}%

%{Phalcon_DI_3258b033b0469f4344bca226b300663b}%

%{Phalcon_DI_0835f3f89ef8c3109362d3c6ae9c7a27}%

%{Phalcon_DI_8050fa4f7661bf8db2f9a6cf47a1d908|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_d32192e4aa1620230493efd07507a036}%

%{Phalcon_DI_50a559a1e1a09e6b3b5c6d451e1ec8e8|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_4168b228435201e1c21c5f9abb8f3f28}%

%{Phalcon_DI_3731f55d6ebf80835d561071e2268e65}%

%{Phalcon_DI_95d334afb740c3f0f3d417fb410e1dc2}%

%{Phalcon_DI_2ad8d47fafbd2a65294fa9aa4497c33f}%

%{Phalcon_DI_19182972f82e522e5efba84a14b57bc5}%

%{Phalcon_DI_3b3b383f1dfee3a85fb1212e54de5f65}%

%{Phalcon_DI_9898af756c69a399d3187da346fe25ee}%

%{Phalcon_DI_c4e3d3b627ce58d3cea0644434a2393f}%

%{Phalcon_DI_0bfccbc8a46df8525956efc2b3759cc1}%

%{Phalcon_DI_6f927afd57568c6bf2548db2276b417c|:doc:`Phalcon\\DI\\Service <Phalcon_DI_Service>`}%

%{Phalcon_DI_3f570b0d3f0e849a87d6ae771abf7ae5}%

%{Phalcon_DI_d8ab3243f71cba09cfbc69462ad54331|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_c5307d09bac945bb6e36aeb19eaf0395}%

%{Phalcon_DI_abd6414e8a6a9f005e7b7f06e1db97b1|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_64d8e5a7725ea3061f1e1fb50e58e7a2}%

%{Phalcon_DI_2ce62b51773f221fe3c33e72402b4938}%

%{Phalcon_DI_76ca6388e9a3681f681fb632077226b6}%

%{Phalcon_DI_8bc576712d881268dbef3d86a141e59c|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_e037e940a2f8bfb2b292ded1cf926c20}%

%{Phalcon_DI_f98e47e205fbccceb8cb48dd751a51c5|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_194ecde9db15b716a22cee811483fddb}%

%{Phalcon_DI_8a8488487206629ea6556a1a83b2d1f2}%

%{Phalcon_DI_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_DI_87480220bbdc87f5b55fced97c5105c6}%

%{Phalcon_DI_936137f836bd590d4dd24d4ca528c18b}%

%{Phalcon_DI_2f89883b4ea5702517b8537bed8db873}%

%{Phalcon_DI_e039ec54f14173293180bfd0b9ff56f9}%

.. code-block:: php

    <?php

    $di['request'] = new Phalcon\Http\Request();





%{Phalcon_DI_d43b6ab79291d64d6f367516b2ec58d4}%

%{Phalcon_DI_3e13ec2ea018ad42c6f00583882ed251}%

.. code-block:: php

    <?php

    var_dump($di['request']);





%{Phalcon_DI_3644e01a19440baa19219140adc68825}%

%{Phalcon_DI_0439e217cf83548ea2dd4a0754c91190}%

%{Phalcon_DI_de674efab64cbe1546d0cc296de016a8}%

%{Phalcon_DI_82c598b86b87b6fac0fe51c6b80a80d0}%

%{Phalcon_DI_6ba774f88a64a7dfe266dcf742a0cb26}%

%{Phalcon_DI_68cf0a3bff1e41b907cf955f570c5249}%

