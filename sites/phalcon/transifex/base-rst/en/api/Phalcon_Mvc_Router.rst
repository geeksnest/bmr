%{Phalcon_Mvc_Router_04ac2e1efddabc429f6bc7a064b31974}%
==============================

%{Phalcon_Mvc_Router_ac486a7bf3dd5c254f532a0626a67c96|:doc:`Phalcon\\Mvc\\RouterInterface <Phalcon_Mvc_RouterInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Router_d40fe2607824cfaf89e236271ccb8a87}%

.. code-block:: php

    <?php

    $router = new Phalcon\Mvc\Router();
    
      $router->add(
    	"/documentation/{chapter}/{name}.{type:[a-z]+}",
    	array(
    		"controller" => "documentation",
    		"action"     => "show"
    	)
    );
    
    $router->handle();
    
    echo $router->getControllerName();




%{Phalcon_Mvc_Router_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Router_35e1f3e13330db4a804fe23eb5719b8d}%

%{Phalcon_Mvc_Router_0db2857a54ac9b47b8f5b9fb24e084c0}%

%{Phalcon_Mvc_Router_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Router_5e81e0738bdffa4f7107bf7e07666094}%

%{Phalcon_Mvc_Router_d9ef0d1963c00de67c0b29fcf6398005}%

%{Phalcon_Mvc_Router_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Router_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Mvc_Router_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Router_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_Router_b3e6298bb197b5403bb793fcfca6ba08}%

%{Phalcon_Mvc_Router_63e633b52b68f4e941aff5e61ccfbac3}%

%{Phalcon_Mvc_Router_7891165f34047553a595aca778dd8923|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_1e659dd8ac820457014ab3db8d16336a}%

.. code-block:: php

    <?php

    $router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);





%{Phalcon_Mvc_Router_bb076d136d5438decd98f6c27265ef0a|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_9ce76f9ec221486accf4a189909d8eb0}%

%{Phalcon_Mvc_Router_06bb110612756e7435a46720073eec8d|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_f8961d275872562f2af28060a262fd04}%

%{Phalcon_Mvc_Router_9b77cca113aa79d98e4d128ee3f5cf7d}%

%{Phalcon_Mvc_Router_7eebb99d9ca319b62c07d8964bbd7342}%

%{Phalcon_Mvc_Router_e59117fda3a9191748bf0e7dd8ffb913|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_d6dc30b314ef7afb8dbf713d2f52ef81}%

%{Phalcon_Mvc_Router_26e3314787194c948c848fba815d43c9}%

%{Phalcon_Mvc_Router_417a8251e9314b6645fb491639a78ee1}%

%{Phalcon_Mvc_Router_bbdd518a5e40347a8ab7cda7c3f7f441|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_1bddb8ba3418530b9db767f0161fc12d}%

%{Phalcon_Mvc_Router_7dea1a036402d9121e4ddd91676a6a89}%

%{Phalcon_Mvc_Router_16ad75cb10aac90324071cb4369e7c2a}%

%{Phalcon_Mvc_Router_31b01ca7e5e39a92c3b55405962af8b0|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_2c676539e85e0e19da7b13c5801ba8a2}%

%{Phalcon_Mvc_Router_f36b15939fcadd33a17f8fe15e3abd4e}%

%{Phalcon_Mvc_Router_a94e66fc01b7c8d78bc6aa590b478337}%

%{Phalcon_Mvc_Router_999c06f9483f91067ca2530e2a40bf94|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_e723bfca7c4d8c15382e91e34fda5957}%

.. code-block:: php

    <?php

     $router->setDefaults(array(
    	'module' => 'common',
    	'action' => 'index'
     ));





%{Phalcon_Mvc_Router_540fb76de760e820c686c0d0c67972f1}%

%{Phalcon_Mvc_Router_cabb62d96fd0834fa8ef5102da79ba95}%

%{Phalcon_Mvc_Router_d769b6ffce91447d799d522b1d93ca6a}%

%{Phalcon_Mvc_Router_25625816543caf8f060f766c1fe385ea}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Router_c9b2282df5f84c46bfd36596a4b81a26%}
     $router->handle();
    
     //{%Phalcon_Mvc_Router_a9c6ae3f6243cf47a26c9ae0072dbee9%}
     $router->handle('/posts/edit/1');





%{Phalcon_Mvc_Router_0dd860fc275c43dc36c8624798d372b1|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_c8bd4141a06a69a156f2682eb6931d5d}%

.. code-block:: php

    <?php

     $router->add('/about', 'About::index');





%{Phalcon_Mvc_Router_6e2de8a07449ba21737b3b2879685370|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_f74b99e1e164260f7718bb4f740c0c71}%

%{Phalcon_Mvc_Router_d2085dd45e82f7d07f095d85c9a32c08|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_868af429955c0ecbbaa95f7fa5e67133}%

%{Phalcon_Mvc_Router_c66c44d522d93a64b24e47939dcec2f6|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_f32c97339be876b8e10fb86cbe916acc}%

%{Phalcon_Mvc_Router_4d3ebb4bc095ca4f5d655d86b435bc91|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_4d752b0f335109eb99ebea41e9977cbc}%

%{Phalcon_Mvc_Router_2138534cbfdbaee4ddbb5b1823b928c8|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_1cf981139831fbc2661c7e1ba2532c59}%

%{Phalcon_Mvc_Router_15ce7b896ca7cd705f83372e6b62e50f|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_c5e8d939bd9ee0fae4a2047ea3f17f18}%

%{Phalcon_Mvc_Router_6c81ff964c4890984ea0cf05a8660167|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_58a4300721cf5feeb2c0899372a9c900}%

%{Phalcon_Mvc_Router_76047c39ac78cbdf7ee19516babdf36b|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_bd6db26afbcc17e967bbde8c762ca756}%

%{Phalcon_Mvc_Router_381b3f641b961d1413101a39a9abdc17|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_78cbcb15186b08545e4422caeba274ec}%

%{Phalcon_Mvc_Router_d9f1667c8f837f416787af94d5645e96}%

%{Phalcon_Mvc_Router_e9a944524b038eeda5d4cd523610fdd9}%

%{Phalcon_Mvc_Router_fcebbf8bb56c7750271e3eda07bd0f9d}%

%{Phalcon_Mvc_Router_8521921da5d7b8a4ff1c299d5dbcabe0}%

%{Phalcon_Mvc_Router_b0d3976a845e6b6da6452c56c2723b2b}%

%{Phalcon_Mvc_Router_a18dd289bb264d04ea72fc170ec4a372}%

%{Phalcon_Mvc_Router_63721017129ab4ca298b33caa1721e91}%

%{Phalcon_Mvc_Router_2c6fb6907ef9185028578e5a754ce3e1}%

%{Phalcon_Mvc_Router_7653c5621328184a0ef6712f5be7b5b7}%

%{Phalcon_Mvc_Router_b7ccc390683274e26381e292617e5300}%

%{Phalcon_Mvc_Router_d88c3713331c25b77ed605f801609a8c}%

%{Phalcon_Mvc_Router_5192b5f7cbd96678251c53086304699f}%

%{Phalcon_Mvc_Router_ae9d973f591d6ca708657670b141ebcd|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_9d206e2acf5932da14cc0a4c36baf912}%

%{Phalcon_Mvc_Router_de1a690bf1e6159d35ec9e63f5e35c8b}%

%{Phalcon_Mvc_Router_b0021d449f14a9b028855c9f0ecd6f0a}%

%{Phalcon_Mvc_Router_9fa6b53f5de55b3515309f42f379b890}%

%{Phalcon_Mvc_Router_03660350d3ef62328c81fe20934cc21d}%

%{Phalcon_Mvc_Router_b643b249425e78b6adceb7c7fb154319|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_80f418139edc49845c3be611f06f30dc}%

%{Phalcon_Mvc_Router_4150d98199fa5d38eee161d6503a46fb|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_81a2cf161aaba093d2e2d69c2de50fac}%

%{Phalcon_Mvc_Router_a5fb18538a20b95d49f7fdd27072cd4c|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_187eb811f0dd111abca2dcb8421c06cb}%

%{Phalcon_Mvc_Router_0b3a3c7fc4b45bae9d11847f97a8edb4}%

%{Phalcon_Mvc_Router_36f78689a8bba7953326923297109939}%

