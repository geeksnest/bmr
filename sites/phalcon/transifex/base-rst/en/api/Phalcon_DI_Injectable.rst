%{Phalcon_DI_Injectable_c578c2a4f7d656ba5541e6ffa4a23b4b}%
==========================================

%{Phalcon_DI_Injectable_25154160f51d01df0ffad59584cfed99|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`}%

%{Phalcon_DI_Injectable_b643f51e6a3fb9a32935a7b85e06c77d}%

%{Phalcon_DI_Injectable_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_DI_Injectable_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_Injectable_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_DI_Injectable_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_Injectable_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_DI_Injectable_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_DI_Injectable_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_DI_Injectable_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_DI_Injectable_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_DI_Injectable_e53e39ef9124fdaf502df86665d52fe5}%

%{Phalcon_DI_Injectable_a8ec8322461cb1dce1953c9378484594}%

