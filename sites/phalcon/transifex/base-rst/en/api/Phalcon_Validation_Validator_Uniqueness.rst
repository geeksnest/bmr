%{Phalcon_Validation_Validator_Uniqueness_9641d006c6dbd41f4d43f946772b63d8}%
====================================================

%{Phalcon_Validation_Validator_Uniqueness_24302afb0327181552d5097efe21d21a|:doc:`Phalcon\\Validation\\Validator <Phalcon_Validation_Validator>`}%

%{Phalcon_Validation_Validator_Uniqueness_3bc33f5008c906bbf75c850085f3e576|:doc:`Phalcon\\Validation\\ValidatorInterface <Phalcon_Validation_ValidatorInterface>`}%

%{Phalcon_Validation_Validator_Uniqueness_056fb609ee74cbd802f15e3351296af4}%

.. code-block:: php

    <?php

    use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
    
    $validator->add('username', new UniquenessValidator(array(
        'model' => 'Users',
        'message' => ':field must be unique'
    )));

  Different attribute from the field 

.. code-block:: php

    <?php

    $validator->add('username', new UniquenessValidator(array(
        'model' => 'Users',
        'attribute' => 'nick'
    )));




%{Phalcon_Validation_Validator_Uniqueness_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Validator_Uniqueness_9a9ac34e57881f672b6419517f5ca372}%

%{Phalcon_Validation_Validator_Uniqueness_d01ba9899fa892ec2b62154529dce37f}%

%{Phalcon_Validation_Validator_Uniqueness_5009115a2e23762a2cebed7a090308f5}%

%{Phalcon_Validation_Validator_Uniqueness_ec865eb22d34722d5b3dfe1c2403678d}%

%{Phalcon_Validation_Validator_Uniqueness_fbdf133d897ae25a2586e5ee3f08ccce}%

%{Phalcon_Validation_Validator_Uniqueness_13f58da2ee2f606006004d67f4b648cc}%

%{Phalcon_Validation_Validator_Uniqueness_08d6177f98e66f04346768bf61e2e6ca}%

%{Phalcon_Validation_Validator_Uniqueness_f2c1e7b80644ef8d34a17b0c2a367fff}%

%{Phalcon_Validation_Validator_Uniqueness_ba312e603be421ffda5e8b7ddba88d42}%

%{Phalcon_Validation_Validator_Uniqueness_2e7772bfbad31074c960e8f21b33e650}%

