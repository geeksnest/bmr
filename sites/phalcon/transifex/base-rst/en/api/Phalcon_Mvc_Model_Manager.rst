%{Phalcon_Mvc_Model_Manager_a2b4eb39129a8170b513b67bd06f8b05}%
======================================

%{Phalcon_Mvc_Model_Manager_00a4a2e053e80d5ddb6eb23473a9648b|:doc:`Phalcon\\Mvc\\Model\\ManagerInterface <Phalcon_Mvc_Model_ManagerInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`}%

%{Phalcon_Mvc_Model_Manager_c00834591324cb747336a73b8dc44114}%

.. code-block:: php

    <?php

     $di = new Phalcon\DI();
    
     $di->set('modelsManager', function() {
          return new Phalcon\Mvc\Model\Manager();
     });
    
     $robot = new Robots($di);




%{Phalcon_Mvc_Model_Manager_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Manager_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Manager_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Model_Manager_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Manager_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Mvc_Model_Manager_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Model_Manager_a47475f9da7bf2fe8a31ee2c6d9c8aff}%

%{Phalcon_Mvc_Model_Manager_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Model_Manager_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Mvc_Model_Manager_55fcba20eaa7d55e13343438d205a095|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Model_Manager_6f33293960d052d3768dc3059dc5491b}%

%{Phalcon_Mvc_Model_Manager_00862d83f1e9a21a66f644b959bfef95|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_3864b73bbb521140066799837212cb9a}%

%{Phalcon_Mvc_Model_Manager_7021ad93a86262bb411916961ec8b573|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_07292c08f64ef28e5aa076b3eec53ad2}%

%{Phalcon_Mvc_Model_Manager_77cbfb0063227b0eead190619a684ff1}%

%{Phalcon_Mvc_Model_Manager_c52ee46fe0a73bff1a27e102d1c0f49c}%

%{Phalcon_Mvc_Model_Manager_da8a87fc25b1e1cc1b9aded3ea88d3c4|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_194105413cb46513c240f5424d73bca3}%

%{Phalcon_Mvc_Model_Manager_ef54d36270e7d785d0ae3ea2e42b21c2|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_953e0302618775be05eb68f805da053b}%

%{Phalcon_Mvc_Model_Manager_f09038cc6aa610bf21eaa61948ab38e5|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_6c41fe7d6fcacbbaaa27c408da0d0c8a}%

%{Phalcon_Mvc_Model_Manager_035ef459a791e8819c00ef4744dd6bb8|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_ecefb95e0aae3563284d79187489456b}%

%{Phalcon_Mvc_Model_Manager_03a0fb68e6b96eb900cb113b36907754|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_cc265115092887faea23aa753928b56e}%

%{Phalcon_Mvc_Model_Manager_627c3c63815ab51e5dc6a1875e80c892|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_b9ea81a20d0a1dc83d0638957eb77cc7}%

%{Phalcon_Mvc_Model_Manager_28c043abeff56a4569ba08326afb05c8|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_ff367a0f2e662d604274d68fadcf9972}%

%{Phalcon_Mvc_Model_Manager_d88214b9acff69d8493cd70422f74384|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_f5c7dde972b0eac83f2c5958e72a3830}%

%{Phalcon_Mvc_Model_Manager_61c2740334f2838c8992fa520dd229ee|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_f9589d7b59c3a8d53c11d17ba9fad9f5}%

%{Phalcon_Mvc_Model_Manager_8846a5487838cf734687f1fc9dca0565|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_92b2f87e475025cb0778e30f05518252}%

%{Phalcon_Mvc_Model_Manager_c4256345a2169aa899b98adced7e9ec9|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_d2c7a58e490faf4e107a83f040b73b37}%

%{Phalcon_Mvc_Model_Manager_38a0067113276541790ee800416ab422|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_0c8847cbac2793afd4d266c1d018bfd5}%

%{Phalcon_Mvc_Model_Manager_29068d98dc3a4998edb31fc058cb423b|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_7813a6309ac6d718883e4a7ef6834df7}%

%{Phalcon_Mvc_Model_Manager_54eac3590eaff3fc5d079f8691fa4da4|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_5ab646fcc0177d33b88cec54073f262e}%

%{Phalcon_Mvc_Model_Manager_f3925c34feccd015a232dabd291699e3|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_6927ac17a3971944b4ec96376e7287e1}%

%{Phalcon_Mvc_Model_Manager_6ea278e4e1d18b2aff06bf5a83ace15f|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`|:doc:`Phalcon\\Mvc\\Model\\BehaviorInterface <Phalcon_Mvc_Model_BehaviorInterface>`}%

%{Phalcon_Mvc_Model_Manager_0993a2571de7624c1ba1bb2e000c261b}%

%{Phalcon_Mvc_Model_Manager_ba31cf2464f5a4767319271276624423|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_91e154c835838793ba43a07ff614fb94}%

%{Phalcon_Mvc_Model_Manager_9c12803864bba7d041697ee029c9d08a}%

%{Phalcon_Mvc_Model_Manager_016c71ff1337c73fb9dca5535a314ecc}%

%{Phalcon_Mvc_Model_Manager_8f3ddf2d4f2ea97f8bf62877da3635a0|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_eb9fa8e24ff9cb826a33ae7c64126527}%

%{Phalcon_Mvc_Model_Manager_1c8cdf172d217d7f19f52939b97c2ec9}%

%{Phalcon_Mvc_Model_Manager_d95c8d4be7dcfe43d30c929fe5afd342}%

%{Phalcon_Mvc_Model_Manager_82c4a536bd6f4811143c9b9b04756841|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_5ba5980d7cf29307f28789f5d949aae5}%

%{Phalcon_Mvc_Model_Manager_8b04680002f484653772d62039e8d3a7|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_ff38b239c295f4e86fb37f385ccf7410}%

%{Phalcon_Mvc_Model_Manager_2c8ea5988fe2fcb2b54b6c204a76504d|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_ad343ba2a8633b9021565dec5a91cb49}%

%{Phalcon_Mvc_Model_Manager_cdd7fea5ce82d761e8698e5a5a205ff3|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`}%

%{Phalcon_Mvc_Model_Manager_b49373ff08fa569f1e0738e5d5a079a8}%

%{Phalcon_Mvc_Model_Manager_30c1ab8f4882dab8aac30cf392626ad7}%

%{Phalcon_Mvc_Model_Manager_2d18b3d6036b432853d0dd6ddb56db8a}%

%{Phalcon_Mvc_Model_Manager_5229c641f14496ab7e72fe9552d399d1}%

%{Phalcon_Mvc_Model_Manager_78a85feda45d2f9136d364dd5365fd0e}%

%{Phalcon_Mvc_Model_Manager_082a1e9050120db7c960f49953047635}%

%{Phalcon_Mvc_Model_Manager_c6363e57e5b26e1a5e13b3f749e9b00b}%

%{Phalcon_Mvc_Model_Manager_718641a01536d0be671de8a31ae55a32}%

%{Phalcon_Mvc_Model_Manager_f8883d0adb290d12f5f868d91f6928f7}%

%{Phalcon_Mvc_Model_Manager_9e893b7ea1c6f2418734a208805a1bd0|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`}%

%{Phalcon_Mvc_Model_Manager_403472febcc5b7ab4e847289a292117c}%

%{Phalcon_Mvc_Model_Manager_d747847d4ac02b19ccf1b422157b8388|:doc:`Phalcon\\Mvc\\Model\\Resultset\\Simple <Phalcon_Mvc_Model_Resultset_Simple>`|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_2656e49d0fcfbb91f3267e4d7c0a6813}%

%{Phalcon_Mvc_Model_Manager_5263ac36ca5e2cbdebe197f1aa57aa39}%

%{Phalcon_Mvc_Model_Manager_94da81465606b42a27053aed3afa9e48}%

%{Phalcon_Mvc_Model_Manager_0caba896686284e4564f70e395059b02}%

%{Phalcon_Mvc_Model_Manager_5bda7c3fe0cf95b4852a85c440f2abf8}%

%{Phalcon_Mvc_Model_Manager_9c0a3ecd17352be2457d608944f77e3a}%

%{Phalcon_Mvc_Model_Manager_50e7c528e932abecf3db6ae871103e0e}%

%{Phalcon_Mvc_Model_Manager_007ea597b152028372c557d7153ce4ae|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_ceed518a7eeb5fda30e4516f6229fe1e}%

%{Phalcon_Mvc_Model_Manager_bcf72c5cd81fc8a883a80b7b6dd3ee09|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_b3d8f020bc868a134c401da8a9f13237}%

%{Phalcon_Mvc_Model_Manager_b3afd3f9c8a5d119053ef913192bd8a9|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Manager_ceed518a7eeb5fda30e4516f6229fe1e}%

%{Phalcon_Mvc_Model_Manager_a3d96d5c54301d30c289dc1e52dc783f|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_44750815d6e84ac024ecb1de2b4f649e}%

.. code-block:: php

    <?php

    $relations = $modelsManager->getBelongsTo(new Robots());





%{Phalcon_Mvc_Model_Manager_6f11649799f9e3632e1bc1b69c9a30ed|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_5da033024213362b4d5299b7f4e112b2}%

%{Phalcon_Mvc_Model_Manager_a868ebceb32c18070a6dbb02d4818005|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_4342059e9b0efd979e30f16588d43717}%

%{Phalcon_Mvc_Model_Manager_a9b37d97546a9bd8fd6f2c98cd7cd99f|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_4ed76530e3a5799a62ea0db7b8c19551}%

%{Phalcon_Mvc_Model_Manager_2afb7caf54c7fe4b148fdc7ab2e793de|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Manager_4342059e9b0efd979e30f16588d43717}%

%{Phalcon_Mvc_Model_Manager_72e953573d3ebf3fd1f3e35035f5e9d8|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`}%

%{Phalcon_Mvc_Model_Manager_41ca42d487202952891e816a2b2dccd4}%

%{Phalcon_Mvc_Model_Manager_21cbc9409c79147c4c2e31bafa447173|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`}%

%{Phalcon_Mvc_Model_Manager_82f3eab93251653d6b1c7201940c2bb1}%

%{Phalcon_Mvc_Model_Manager_a25d8c18c9cb4a5a15cf326970ec86df|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`}%

%{Phalcon_Mvc_Model_Manager_c4888a8c5c5b6d87167aa38f2e660331}%

%{Phalcon_Mvc_Model_Manager_6bc4476401f5fc749aeae6ecab4a2ec7|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`}%

%{Phalcon_Mvc_Model_Manager_567b3a0b16e3ab59deebe44f90a148d1}%

%{Phalcon_Mvc_Model_Manager_e97634a1afd677d255a970c7f88b0568|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Manager_d2afc31533b1a6f2d20e8223042b77d6}%

%{Phalcon_Mvc_Model_Manager_ffdb35999e086bd5a3c5b1cd1c974189|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`}%

%{Phalcon_Mvc_Model_Manager_84e24f4754fa760310a979254266c91e}%

%{Phalcon_Mvc_Model_Manager_6fa0b7a639250dda9c7edd14e86f8d18}%

%{Phalcon_Mvc_Model_Manager_054729b1aec137578787727c6997008a}%

%{Phalcon_Mvc_Model_Manager_8eabf8d3be6d2924064079f1f581649d}%

%{Phalcon_Mvc_Model_Manager_8644b9c62ef0f3ae49d464ca8826b6d8}%

%{Phalcon_Mvc_Model_Manager_a91d8fcde41196a5d6b04e914e54f093}%

%{Phalcon_Mvc_Model_Manager_a19f9eaa99a2cccf77881be5aa3e60b7}%

%{Phalcon_Mvc_Model_Manager_4efdfc6db0248d6e3f98218f90fe9b22}%

%{Phalcon_Mvc_Model_Manager_95c50667ffac9bdd83a0b6642ddcb21f}%

