%{Phalcon_Mvc_Model_Query_BuilderInterface_4b4d9ae44210038ea6e3ffad19e20a3d}%
==========================================================

%{Phalcon_Mvc_Model_Query_BuilderInterface_a08f6ff75775d74477f840012d1155bc}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Query_BuilderInterface_39a45090a34d1cdddabc24b51c7a3148}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_f6aae874b84451bfee78192581595210}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_a3fa1d8f606abe14cd4a5bee9c554735|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_fc719f15beaba4531845835f3506a242}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_344fd92b36dc61e91d1703f715f7cefe}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_69644b0c70dc6a1a1aff55d19698eaa2}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_51cbbf0e93e5194fa044ba1418dc9f23|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_5e3fa125f554d365c03ebfe65d8eb2c1}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_601e2079c2d9249171c95a9065d3f426|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_bc4821fa209d7a9ea9c6c997e28a894f}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_b10ab374095e92e2c9a7635b1ed11dc1}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_fa873d26c84eb65fd83dbc02bf0a9ff5}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_7e4e6fc2dcd87567c445808a94c6a440|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_5edd0d8119eca5ae704a5c5acee6b049}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_e49a55abceed1ccc5aec7d4a37816cb8|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_5edd0d8119eca5ae704a5c5acee6b049}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_4341e776bc5131455ab1c9f2208c84db|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_d182ed17c06a1f090c41fea03f9d9e3a}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_6284748a051bcf4d012bf0219ba67c0f|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_757b8c892710a0e611c275bc844e33bc}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_5f91a566c4843a9ede379576265b283e|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_750389464787afa02f467cd978026eac}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_25201f5aa0e954094810441ce4f68759|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_df3b67bd1b568c18c832290a8d872cb3}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_7e9d3bc8cbb97fd342be4f5263b6edbc|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_2e648581cd85405f0743c23afc6e966a}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_7bfed6458238f8d00fd58c8a5c094f78|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_6d9f0ea884a09db091690c2d05e52d3c}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_95fc2bba995fd1c19f925f3ce6a0f86c|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_3af635042b716a4dc8151c2a92beedff}%

.. code-block:: php

    <?php

    $builder->notBetweenWhere('price', 100.25, 200.50);





%{Phalcon_Mvc_Model_Query_BuilderInterface_a685a6053649cab6e9dac6a57af5575d|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_de3e89ea07cf9239062d326c5c2ac92d}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_9267f18e31537c76d5c7c729d18c01c4|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_58cbb59f7f6417ef48c5718a8a45f28f}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_0885144dd3dd16d89ffd108141af3cba}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_e16b11297c09d247829b72e3939ec90c}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_8fdfa82a5f6f868e0b774e8d3d44f9e7|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_f83c8b2306bd643b51d855b3f360a592}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_355e317cf1839896fa940df31319f96d}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_6e55dfad153ac2f5da1659052b2cfd2c}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_b9008fe31d37efde3764228cf6506539|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_ff6785529053f6844665907982fd551e}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_3d5ec1401cad70ac8a596d36fdf5f7d3}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_20f521cadea74c4ce6c129eb97af79f8}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_473b6dc91ee8d9f84ef24a41cfc606a4|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_b8a90af21c7f5dcb10fd11da0d59ac0b}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_e22f13b22705d6f1d4bde506df766ceb}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_7649637bec95e5a9e667b6b10c14aae1}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_3f66f8504d1bd36e73f53a1d2f640aa7|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_b8a90af21c7f5dcb10fd11da0d59ac0b}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_f25e10e762d65b42ee0b1d8b00ec76b5}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_954b2a1161fae1f6ab8a18f5d9ddf137}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_3b08eced55d1cf967d9d16dfd3852bf5}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_fbed73a8112f4a2fa839cc97dccc5799}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_a25f61e0e2f43b2b0b0b2c777704f208|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`}%

%{Phalcon_Mvc_Model_Query_BuilderInterface_66d053994e67a0f80fd133d8c4319d47}%

