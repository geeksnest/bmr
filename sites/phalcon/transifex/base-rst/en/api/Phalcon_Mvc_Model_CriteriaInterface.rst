%{Phalcon_Mvc_Model_CriteriaInterface_95bd59d07c2cd4ef21b438bb91f62d30}%
====================================================

%{Phalcon_Mvc_Model_CriteriaInterface_56149674bb6ca942232c8802b24a7a73}%

%{Phalcon_Mvc_Model_CriteriaInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_CriteriaInterface_9a8bb4dba5cf301a710f0cf58d606d98|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_d277e11970321de856229a8d9ed40c0c}%

%{Phalcon_Mvc_Model_CriteriaInterface_5ac30f9be154a5cb24b00ca9e87a6736}%

%{Phalcon_Mvc_Model_CriteriaInterface_a8f9b978b66048b26dc90f106b1aed05}%

%{Phalcon_Mvc_Model_CriteriaInterface_e3549bf65bc1970fda913f271c8a9407|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_c15575426b24137f5b8efb1e890a365e}%

%{Phalcon_Mvc_Model_CriteriaInterface_e619737ee9431b35322be18e6d6f1d2a|:doc:`Phalcon\\Mvc\\Model\\Criteria <Phalcon_Mvc_Model_Criteria>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_1251b9fba79de88a3016fb2719dbd1c5}%

%{Phalcon_Mvc_Model_CriteriaInterface_11a59910e8847bf996564079ff73f1fe|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_2574003a9b19529266aaa61b06ab4f51}%

.. code-block:: php

    <?php

    $criteria->columns(array('id', 'name'));





%{Phalcon_Mvc_Model_CriteriaInterface_c397403cdccd3d023cb9b52747838457|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_33ea061efdab4257fe7703fec27206db}%

.. code-block:: php

    <?php

    $criteria->join('Robots');
    $criteria->join('Robots', 'r.id = RobotsParts.robots_id');
    $criteria->join('Robots', 'r.id = RobotsParts.robots_id', 'r');
    $criteria->join('Robots', 'r.id = RobotsParts.robots_id', 'r', 'LEFT');





%{Phalcon_Mvc_Model_CriteriaInterface_a2d931a239c6dded9389d99230443640|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_c984c2db513e3aee8ff150c0a2895dfc}%

%{Phalcon_Mvc_Model_CriteriaInterface_71fbdd176b9ec55099d00112e19ec8db|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_c984c2db513e3aee8ff150c0a2895dfc}%

%{Phalcon_Mvc_Model_CriteriaInterface_2c0576029618960ede813286ea407eb7|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_ab755017f6060bf47e715db2cfb87677}%

%{Phalcon_Mvc_Model_CriteriaInterface_98e4ace7abfe52e23a25ad5595f746db|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_0a236fe9a842cb7b5560881c11966041}%

%{Phalcon_Mvc_Model_CriteriaInterface_eb5770fbb67b9474c4e05257f296d4f4|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_7f83751202d396c44026b6c836eb2720}%

%{Phalcon_Mvc_Model_CriteriaInterface_719a7951614920d3e856c531c07db11c|:doc:`Phalcon\\Mvc\\Model\\Criteria <Phalcon_Mvc_Model_Criteria>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_0900d116670bf93b9cd27fdccdf1d89c}%

%{Phalcon_Mvc_Model_CriteriaInterface_2935eab22dbf505a5653a75b1dde08ae|:doc:`Phalcon\\Mvc\\Model\\Criteria <Phalcon_Mvc_Model_Criteria>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_25a5e11da22e8d51962c605a18eb59f4}%

%{Phalcon_Mvc_Model_CriteriaInterface_a1a1944216a1573349d65eba393a1927|:doc:`Phalcon\\Mvc\\Model\\Criteria <Phalcon_Mvc_Model_Criteria>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_3501b6256c9b59bfad956846d1f11672}%

%{Phalcon_Mvc_Model_CriteriaInterface_7bfed6458238f8d00fd58c8a5c094f78|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_053efd36d40c71c6d89b754bcb77aed4}%

.. code-block:: php

    <?php

    $criteria->betweenWhere('price', 100.25, 200.50);





%{Phalcon_Mvc_Model_CriteriaInterface_95fc2bba995fd1c19f925f3ce6a0f86c|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_3af635042b716a4dc8151c2a92beedff}%

.. code-block:: php

    <?php

    $criteria->notBetweenWhere('price', 100.25, 200.50);





%{Phalcon_Mvc_Model_CriteriaInterface_a685a6053649cab6e9dac6a57af5575d|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_fa989e42243a0cf3eda9b98cb8a995f3}%

.. code-block:: php

    <?php

    $criteria->inWhere('id', [1, 2, 3]);





%{Phalcon_Mvc_Model_CriteriaInterface_9267f18e31537c76d5c7c729d18c01c4|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_598624af0745dc322869bd1b14451f20}%

.. code-block:: php

    <?php

    $criteria->notInWhere('id', [1, 2, 3]);





%{Phalcon_Mvc_Model_CriteriaInterface_57751366afb7107a18ac9807e3993b51}%

%{Phalcon_Mvc_Model_CriteriaInterface_aeb284dff1578faaf648730a331b53c6}%

%{Phalcon_Mvc_Model_CriteriaInterface_8cd45f60486fd6a29ac5e66a192f7583}%

%{Phalcon_Mvc_Model_CriteriaInterface_aeb284dff1578faaf648730a331b53c6}%

%{Phalcon_Mvc_Model_CriteriaInterface_ffba03cf91d21c3ee46b96b215935f83}%

%{Phalcon_Mvc_Model_CriteriaInterface_18dad9bdbf6a3e89ab512e375857f855}%

%{Phalcon_Mvc_Model_CriteriaInterface_c7e56165d9a720ed48e291e7d43ad680}%

%{Phalcon_Mvc_Model_CriteriaInterface_3320e4f14546508c861103a26ae5915a}%

%{Phalcon_Mvc_Model_CriteriaInterface_5cfc6a9c16f255699df2144249cc46e9}%

%{Phalcon_Mvc_Model_CriteriaInterface_ee97bc1aea0633aaa81c27ee4b0e0d80}%

%{Phalcon_Mvc_Model_CriteriaInterface_19b430552e205da2529ce632f3f4d9dd|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_930a07dbb8cd29e8eee2b89434b66e28}%

%{Phalcon_Mvc_Model_CriteriaInterface_1808f1f97dcdafa237fdf927511c0f4a|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_CriteriaInterface_f1d2972271f1db28ac895f823169d1fa}%

