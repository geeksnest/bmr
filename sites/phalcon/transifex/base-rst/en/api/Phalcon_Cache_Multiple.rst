%{Phalcon_Cache_Multiple_d58c7b1f7c998c2fdf93875364c3a640}%
==================================

%{Phalcon_Cache_Multiple_83c6769b2d56f94102fdf3a71f526046}%

.. code-block:: php

    <?php

       use Phalcon\Cache\Frontend\Data as DataFrontend,
           Phalcon\Cache\Multiple,
           Phalcon\Cache\Backend\Apc as ApcCache,
           Phalcon\Cache\Backend\Memcache as MemcacheCache,
           Phalcon\Cache\Backend\File as FileCache;
    
       $ultraFastFrontend = new DataFrontend(array(
           "lifetime" => 3600
       ));
    
       $fastFrontend = new DataFrontend(array(
           "lifetime" => 86400
       ));
    
       $slowFrontend = new DataFrontend(array(
           "lifetime" => 604800
       ));
    
       //{%Phalcon_Cache_Multiple_00bc8a652749364a924e67bd2631f979%}
       $cache = new Multiple(array(
           new ApcCache($ultraFastFrontend, array(
               "prefix" => 'cache',
           )),
           new MemcacheCache($fastFrontend, array(
               "prefix" => 'cache',
               "host" => "localhost",
               "port" => "11211"
           )),
           new FileCache($slowFrontend, array(
               "prefix" => 'cache',
               "cacheDir" => "../app/cache/"
           ))
       ));
    
       //{%Phalcon_Cache_Multiple_8376c010cf364f6099bd9ec2242f95f0%}
       $cache->save('my-key', $data);




%{Phalcon_Cache_Multiple_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Multiple_ae8eb70247a146fc6f240f702d8ddc9f}%

%{Phalcon_Cache_Multiple_91baa91e9176942f855125f40cbbfe68}%

%{Phalcon_Cache_Multiple_7a26534739d95dd8a601480ef4ec355e|:doc:`Phalcon\\Cache\\Multiple <Phalcon_Cache_Multiple>`|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Cache_Multiple_45acdb2c6a7fff3b2b357a203a41a727}%

%{Phalcon_Cache_Multiple_90af1871aa697ede5d04ce5b7841dded}%

%{Phalcon_Cache_Multiple_083566941e02dad8935d2f6fc71366ea}%

%{Phalcon_Cache_Multiple_6e4f7d3ef36f37b2d7be335af1fc38b6}%

%{Phalcon_Cache_Multiple_bdb0e90feda549ae0cc7871593c0ff0f}%

%{Phalcon_Cache_Multiple_289785ad024d50b18a3dfa7e6a53b280}%

%{Phalcon_Cache_Multiple_ecebfac342142e7dcde17857b5bb536b}%

%{Phalcon_Cache_Multiple_92a86099a531589aac19438e058b04bf}%

%{Phalcon_Cache_Multiple_66dbcf8523002e0b5bf356ca83aca544}%

%{Phalcon_Cache_Multiple_1802823d299420e98af3a958b7d763e9}%

%{Phalcon_Cache_Multiple_02fda1866a1ec429ee4d68e61b2b30ce}%

