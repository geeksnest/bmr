%{Phalcon_Mvc_Model_Criteria_98dac7ef6f92459dba7d98f654fb72de}%
=======================================

%{Phalcon_Mvc_Model_Criteria_de8605e7f95b9452cea1b6de82ec2fa9|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Model_Criteria_0e1ba685f64ab74f8cef2697fc369218}%

.. code-block:: php

    <?php

    $robots = Robots::query()
        ->where("type = :type:")
        ->andWhere("year < 2000")
        ->bind(array("type" => "mechanical"))
        ->order("name")
        ->execute();




%{Phalcon_Mvc_Model_Criteria_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Criteria_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Criteria_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Model_Criteria_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Criteria_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Mvc_Model_Criteria_966da29b8aa6f52195e6b8ae9960f30f|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_d277e11970321de856229a8d9ed40c0c}%

%{Phalcon_Mvc_Model_Criteria_8d8d87229aed304e6b536d8ecdb22515}%

%{Phalcon_Mvc_Model_Criteria_a8f9b978b66048b26dc90f106b1aed05}%

%{Phalcon_Mvc_Model_Criteria_87f4e6d37bc5c1214cabfb556e7eb90d|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_fe4829cb59663d2162fefc9c93de3acb}%

%{Phalcon_Mvc_Model_Criteria_b1247de9261bf6b15de734cd947d6bf7|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_1251b9fba79de88a3016fb2719dbd1c5}%

%{Phalcon_Mvc_Model_Criteria_fd310c8d6ce3d906fe591fe30c8cbdca|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_2574003a9b19529266aaa61b06ab4f51}%

.. code-block:: php

    <?php

    $criteria->columns(array('id', 'name'));





%{Phalcon_Mvc_Model_Criteria_6729f5afe3361bfaf8b27bbc4053c631|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_33ea061efdab4257fe7703fec27206db}%

.. code-block:: php

    <?php

    $criteria->join('Robots');
    $criteria->join('Robots', 'r.id = RobotsParts.robots_id');
    $criteria->join('Robots', 'r.id = RobotsParts.robots_id', 'r');
    $criteria->join('Robots', 'r.id = RobotsParts.robots_id', 'r', 'LEFT');





%{Phalcon_Mvc_Model_Criteria_352121f224d06a2147d0f1def24c78a8|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_6dd1274e7fc80f0662f9b10d14f24c35}%

.. code-block:: php

    <?php

    $criteria->innerJoin('Robots');
    $criteria->innerJoin('Robots', 'r.id = RobotsParts.robots_id');
    $criteria->innerJoin('Robots', 'r.id = RobotsParts.robots_id', 'r');
    $criteria->innerJoin('Robots', 'r.id = RobotsParts.robots_id', 'r', 'LEFT');





%{Phalcon_Mvc_Model_Criteria_018d9466c616355cc13c779b00dd41c0|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_afd17acf794d8c93344aef95bca8ba90}%

.. code-block:: php

    <?php

    $criteria->leftJoin('Robots', 'r.id = RobotsParts.robots_id', 'r');





%{Phalcon_Mvc_Model_Criteria_cf7f7a4719eb9e39185b6ba3823a8af0|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_0c13135b6ff4dd75adacce9c19596a31}%

.. code-block:: php

    <?php

    $criteria->rightJoin('Robots', 'r.id = RobotsParts.robots_id', 'r');





%{Phalcon_Mvc_Model_Criteria_ad4ddea7dc9a7d69326bca56da8b2fab|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_2fbd1dc0f7b10cbf4f30b67bce8deffb}%

%{Phalcon_Mvc_Model_Criteria_8f5281ff11d5df52685f1e954724126c|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_eb672a828b43652bdd1fcf1745f12807}%

%{Phalcon_Mvc_Model_Criteria_1ab2b90c2bb91c39c5fb66ba05950812|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_25a5e11da22e8d51962c605a18eb59f4}%

%{Phalcon_Mvc_Model_Criteria_74a87bf4ea7bd7dbcd5b8a61eee31539|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_3501b6256c9b59bfad956846d1f11672}%

%{Phalcon_Mvc_Model_Criteria_d38cc6f1a6acdd832c0847619844ad01|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_053efd36d40c71c6d89b754bcb77aed4}%

.. code-block:: php

    <?php

    $criteria->betweenWhere('price', 100.25, 200.50);





%{Phalcon_Mvc_Model_Criteria_b5bda5babfa20177652e61e748ccee79|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_3af635042b716a4dc8151c2a92beedff}%

.. code-block:: php

    <?php

    $criteria->notBetweenWhere('price', 100.25, 200.50);





%{Phalcon_Mvc_Model_Criteria_6a4625cc8467c6425b468e92de0145a3|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_fa989e42243a0cf3eda9b98cb8a995f3}%

.. code-block:: php

    <?php

    $criteria->inWhere('id', [1, 2, 3]);





%{Phalcon_Mvc_Model_Criteria_098de47a039ef285186b4b7da86c20e6|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_598624af0745dc322869bd1b14451f20}%

.. code-block:: php

    <?php

    $criteria->notInWhere('id', [1, 2, 3]);





%{Phalcon_Mvc_Model_Criteria_88c5a2096f87e1ac9b18c8b5ddeefbdd}%

%{Phalcon_Mvc_Model_Criteria_c984c2db513e3aee8ff150c0a2895dfc}%

%{Phalcon_Mvc_Model_Criteria_4d4778af076b7606ceeb0400c4c3a16d|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_f96ca62b6cbe3d13cab7e73f1d60ea23}%

%{Phalcon_Mvc_Model_Criteria_279fe8c209b105ddc93df7276336b5b9|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_ab755017f6060bf47e715db2cfb87677}%

%{Phalcon_Mvc_Model_Criteria_bd6183bfab0ee675552251557184093f|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_30b386dc6d733418b5ba7063c89d9004}%

%{Phalcon_Mvc_Model_Criteria_90be0043dea576cd98b0c80b50101107|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_1a4edfc65a75dfcc171b5bffdd7acb30}%

%{Phalcon_Mvc_Model_Criteria_93bd8175c1d24d1150e16cd94bb6466c|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_4bd3ca880b12fea5f88aa92614589c71}%

%{Phalcon_Mvc_Model_Criteria_b8375b3107c4b80e5c11417cbbeda87b}%

%{Phalcon_Mvc_Model_Criteria_aeb284dff1578faaf648730a331b53c6}%

%{Phalcon_Mvc_Model_Criteria_62e23edee25721e38b02c774732bae26}%

%{Phalcon_Mvc_Model_Criteria_69644b0c70dc6a1a1aff55d19698eaa2}%

%{Phalcon_Mvc_Model_Criteria_2e00888399c19f06ff371023a2f9e2ee}%

%{Phalcon_Mvc_Model_Criteria_aeb284dff1578faaf648730a331b53c6}%

%{Phalcon_Mvc_Model_Criteria_2d9e23f39991d1ec1f40d28c6656c5f4}%

%{Phalcon_Mvc_Model_Criteria_18dad9bdbf6a3e89ab512e375857f855}%

%{Phalcon_Mvc_Model_Criteria_e1ed359b5b7fa934e43efbb465635f13}%

%{Phalcon_Mvc_Model_Criteria_3320e4f14546508c861103a26ae5915a}%

%{Phalcon_Mvc_Model_Criteria_d88c3713331c25b77ed605f801609a8c}%

%{Phalcon_Mvc_Model_Criteria_ee97bc1aea0633aaa81c27ee4b0e0d80}%

%{Phalcon_Mvc_Model_Criteria_21ab1a2c8ec17c79896ae7107ec8d0d6|:doc:`Phalcon\\Mvc\\Model\\Criteria <Phalcon_Mvc_Model_Criteria>`|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Criteria_930a07dbb8cd29e8eee2b89434b66e28}%

%{Phalcon_Mvc_Model_Criteria_0d0e48df9c372d0c5db18c0dad6d2743|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_Criteria_f1d2972271f1db28ac895f823169d1fa}%

%{Phalcon_Mvc_Model_Criteria_a7e56fdd22c5b4de2ec2b8b4188463c6|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`}%

%{Phalcon_Mvc_Model_Criteria_46b8b2b8c58428d24e623fa1c3f30b2d}%

