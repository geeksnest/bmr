%{Phalcon_Db_Profiler_21f5cff86ef1ea3e0453aa2b91f4d6ee}%
===============================

%{Phalcon_Db_Profiler_e9bdedd52714dd9a5d40d847b795b9be}%

.. code-block:: php

    <?php

    $profiler = new Phalcon\Db\Profiler();
    
    //{%Phalcon_Db_Profiler_af6ab824eb41c133cf71a177e6e16dfb%}
    $connection->setProfiler($profiler);
    
    $sql = "SELECT buyer_name, quantity, product_name
    FROM buyers LEFT JOIN products ON
    buyers.pid=products.id";
    
    //{%Phalcon_Db_Profiler_81e9bd0aa2782b740c867d02541c0325%}
    $connection->query($sql);
    
    //{%Phalcon_Db_Profiler_82d803f6fb4acc664e98cd0e54612fe1%}
    $profile = $profiler->getLastProfile();
    
    echo "SQL Statement: ", $profile->getSQLStatement(), "\n";
    echo "Start Time: ", $profile->getInitialTime(), "\n";
    echo "Final Time: ", $profile->getFinalTime(), "\n";
    echo "Total Elapsed Time: ", $profile->getTotalElapsedSeconds(), "\n";




%{Phalcon_Db_Profiler_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Profiler_99b18b23bf1961577992ed85c6d14de0|:doc:`Phalcon\\Db\\Profiler <Phalcon_Db_Profiler>`}%

%{Phalcon_Db_Profiler_73160b2c8ab0f4dce2ed885185b8cbb7}%

%{Phalcon_Db_Profiler_b2c7f5f441f94842a3e15c2b5ad8afd9|:doc:`Phalcon\\Db\\Profiler <Phalcon_Db_Profiler>`}%

%{Phalcon_Db_Profiler_bb612b73d788575fa4fdb06d5d260435}%

%{Phalcon_Db_Profiler_91e281c9b74077b62317caf8a3be0821}%

%{Phalcon_Db_Profiler_8ed68a9be452d2008c6f4ffcf2591531}%

%{Phalcon_Db_Profiler_ceb5edf693fb1da180036cc52f7451c5}%

%{Phalcon_Db_Profiler_becccadd21f5857b835c84a802607629}%

%{Phalcon_Db_Profiler_7a32c4f2cea1813e95b168cb27bad20f|:doc:`Phalcon\\Db\\Profiler\\Item <Phalcon_Db_Profiler_Item>`}%

%{Phalcon_Db_Profiler_7c1ac81718600dbe7c357c44eef19f59}%

%{Phalcon_Db_Profiler_18d1beb26b6186b1c302016920d4e792|:doc:`Phalcon\\Db\\Profiler <Phalcon_Db_Profiler>`}%

%{Phalcon_Db_Profiler_cf4842bf01c1ec63e1f22dd0cc1a90ae}%

%{Phalcon_Db_Profiler_4beb6efbbb67c00d1ef54c49a7cb55cf|:doc:`Phalcon\\Db\\Profiler\\Item <Phalcon_Db_Profiler_Item>`}%

%{Phalcon_Db_Profiler_52861b754b6d7830c01bb2948d7ec12c}%

