%{Phalcon_Cache_Backend_Xcache_e90d8522cf02e9eb8de4169c5650e776}%
=========================================

%{Phalcon_Cache_Backend_Xcache_797e5d789434d10ddd0a11dc0d3a9e57|:doc:`Phalcon\\Cache\\Backend <Phalcon_Cache_Backend>`}%

%{Phalcon_Cache_Backend_Xcache_c1eac5b56048ee7e86f6a6737e5ede2a|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Cache_Backend_Xcache_9207bb800ce6934d404d60ab69615e3c}%

.. code-block:: php

    <?php

    //{%Phalcon_Cache_Backend_Xcache_77c80c813b109e091cbde98ceccddd0c%}
    $frontCache = new Phalcon\Cache\Frontend\Data(array(
    	'lifetime' => 172800
    ));
    
      $cache = new Phalcon\Cache\Backend\Xcache($frontCache, array(
          'prefix' => 'app-data'
      ));
    
    //{%Phalcon_Cache_Backend_Xcache_f3197c319b2e1bb4ba59291a6ad75fa9%}
    $cache->save('my-data', array(1, 2, 3, 4, 5));
    
    //{%Phalcon_Cache_Backend_Xcache_45b6351e81b39eda563b06fcc2f2b1ff%}
    $data = $cache->get('my-data');




%{Phalcon_Cache_Backend_Xcache_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Backend_Xcache_d0ebe8723e443cd9102587c6adecc491|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Backend_Xcache_c03b75a577083091040b9fcd66084eb8}%

%{Phalcon_Cache_Backend_Xcache_90af1871aa697ede5d04ce5b7841dded}%

%{Phalcon_Cache_Backend_Xcache_32c8725a355ab573b10e33ed519032c3}%

%{Phalcon_Cache_Backend_Xcache_289785ad024d50b18a3dfa7e6a53b280}%

%{Phalcon_Cache_Backend_Xcache_7c48e102c6ab8dd411f3d9ecd1e874e0}%

%{Phalcon_Cache_Backend_Xcache_2ea0d933ca3725409b2cf4c8c44b0dc0}%

%{Phalcon_Cache_Backend_Xcache_5a3c26a229b51db139ac1ff3258ce404}%

%{Phalcon_Cache_Backend_Xcache_d073186049f74018e6ac389752e0231d}%

%{Phalcon_Cache_Backend_Xcache_0496bd63d7dd49a90bc039bf152277c2}%

%{Phalcon_Cache_Backend_Xcache_1802823d299420e98af3a958b7d763e9}%

%{Phalcon_Cache_Backend_Xcache_d85d69ab7af82d5b2ee3f7aa9a01c40c}%

%{Phalcon_Cache_Backend_Xcache_51b9f069d953ee0838b4029d1623b43f}%

%{Phalcon_Cache_Backend_Xcache_b8c86e82904483c0368bc43821b5fc8a}%

%{Phalcon_Cache_Backend_Xcache_458a4b4698c693cac55078739b14aecd}%

%{Phalcon_Cache_Backend_Xcache_5b1102727a9b0b9a3ddfb9de496697ff}%

%{Phalcon_Cache_Backend_Xcache_243f7860634f5addc7cc2cd15ec9587b}%

%{Phalcon_Cache_Backend_Xcache_5fbdd81239a6a360b99505e1ccc4c96a}%

%{Phalcon_Cache_Backend_Xcache_4c1e2a7813fc357bd5b2961fe618cf57}%

%{Phalcon_Cache_Backend_Xcache_5aa5eebbc6e2e0ba1eb669cfcf9314ac}%

%{Phalcon_Cache_Backend_Xcache_f8bbbf10eb7b23d1ad6555244df95daa}%

%{Phalcon_Cache_Backend_Xcache_ee84c5ddac90bc82f87d82edd83f803d}%

%{Phalcon_Cache_Backend_Xcache_20b1a896cfa41a581678813a40ad294b}%

%{Phalcon_Cache_Backend_Xcache_a45d0bbd7f1d2ca9be308e066116571d}%

%{Phalcon_Cache_Backend_Xcache_f4c67e395756ed811b3d86ccd37680d7}%

%{Phalcon_Cache_Backend_Xcache_0a7c1cf9fc6cf16b0c6d365b453b8c9c}%

%{Phalcon_Cache_Backend_Xcache_6e3b53a9e2f02ef15c6e622b43ad7fd6}%

%{Phalcon_Cache_Backend_Xcache_2e2e8a1edb8ee6d249c3408ed9a13506}%

%{Phalcon_Cache_Backend_Xcache_f616a41f5f598297276e1c7ec3a0c216}%

%{Phalcon_Cache_Backend_Xcache_132cc48b9d06f81662724fafbcf5388d}%

%{Phalcon_Cache_Backend_Xcache_25608a5b33db734ab297a041c17d0afe}%

%{Phalcon_Cache_Backend_Xcache_4f309f2cbbf78c9cc272007b4b90e759}%

%{Phalcon_Cache_Backend_Xcache_d95154d7678637473d5bd3651a6b7981}%

%{Phalcon_Cache_Backend_Xcache_63ef827e599ff5e9ce28cac18e0a74de}%

%{Phalcon_Cache_Backend_Xcache_8f54d16975ae8256cc1f314641e21364}%

%{Phalcon_Cache_Backend_Xcache_7b222737002802a81ee0a69bdb497f3a}%

