%{Phalcon_Cache_Backend_Memory_865e1c731fcf565bdeac7609e03a0425}%
=========================================

%{Phalcon_Cache_Backend_Memory_797e5d789434d10ddd0a11dc0d3a9e57|:doc:`Phalcon\\Cache\\Backend <Phalcon_Cache_Backend>`}%

%{Phalcon_Cache_Backend_Memory_c1eac5b56048ee7e86f6a6737e5ede2a|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Cache_Backend_Memory_f4dc2e7c2585740dd7d62237a95d367b}%

.. code-block:: php

    <?php

    //{%Phalcon_Cache_Backend_Memory_002548a4ee53cdcc2acdab6db3d35fef%}
    $frontCache = new Phalcon\Cache\Frontend\Data();
    
      $cache = new Phalcon\Cache\Backend\Memory($frontCache);
    
    //{%Phalcon_Cache_Backend_Memory_f3197c319b2e1bb4ba59291a6ad75fa9%}
    $cache->save('my-data', array(1, 2, 3, 4, 5));
    
    //{%Phalcon_Cache_Backend_Memory_45b6351e81b39eda563b06fcc2f2b1ff%}
    $data = $cache->get('my-data');




%{Phalcon_Cache_Backend_Memory_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Backend_Memory_90af1871aa697ede5d04ce5b7841dded}%

%{Phalcon_Cache_Backend_Memory_3bff3193b42b39b97ac714131c9e7a9c}%

%{Phalcon_Cache_Backend_Memory_289785ad024d50b18a3dfa7e6a53b280}%

%{Phalcon_Cache_Backend_Memory_ec1498d2fc21e1ff07ee37165e50bc64}%

%{Phalcon_Cache_Backend_Memory_2ea0d933ca3725409b2cf4c8c44b0dc0}%

%{Phalcon_Cache_Backend_Memory_5a3c26a229b51db139ac1ff3258ce404}%

%{Phalcon_Cache_Backend_Memory_d073186049f74018e6ac389752e0231d}%

%{Phalcon_Cache_Backend_Memory_0496bd63d7dd49a90bc039bf152277c2}%

%{Phalcon_Cache_Backend_Memory_1802823d299420e98af3a958b7d763e9}%

%{Phalcon_Cache_Backend_Memory_92afc5ac5ca4be06975fc9ff5ba4e195}%

%{Phalcon_Cache_Backend_Memory_576d0381af3fddaaf0f8d1250a822a00}%

%{Phalcon_Cache_Backend_Memory_a07e4d23894db0c2eff391b65acf4d37}%

%{Phalcon_Cache_Backend_Memory_f37c0294f50af10c35857c1980a0bfa0}%

%{Phalcon_Cache_Backend_Memory_3841e3ae4ab991011ff2b535cc9a2e85}%

%{Phalcon_Cache_Backend_Memory_243f7860634f5addc7cc2cd15ec9587b}%

%{Phalcon_Cache_Backend_Memory_5fbdd81239a6a360b99505e1ccc4c96a}%

%{Phalcon_Cache_Backend_Memory_5ffc6dbad51d5e493f2df71553145de0|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Backend_Memory_b05144a8929c0058e9b450700ae709cd}%

%{Phalcon_Cache_Backend_Memory_4c1e2a7813fc357bd5b2961fe618cf57}%

%{Phalcon_Cache_Backend_Memory_5aa5eebbc6e2e0ba1eb669cfcf9314ac}%

%{Phalcon_Cache_Backend_Memory_f8bbbf10eb7b23d1ad6555244df95daa}%

%{Phalcon_Cache_Backend_Memory_ee84c5ddac90bc82f87d82edd83f803d}%

%{Phalcon_Cache_Backend_Memory_20b1a896cfa41a581678813a40ad294b}%

%{Phalcon_Cache_Backend_Memory_a45d0bbd7f1d2ca9be308e066116571d}%

%{Phalcon_Cache_Backend_Memory_f4c67e395756ed811b3d86ccd37680d7}%

%{Phalcon_Cache_Backend_Memory_0a7c1cf9fc6cf16b0c6d365b453b8c9c}%

%{Phalcon_Cache_Backend_Memory_6e3b53a9e2f02ef15c6e622b43ad7fd6}%

%{Phalcon_Cache_Backend_Memory_2e2e8a1edb8ee6d249c3408ed9a13506}%

%{Phalcon_Cache_Backend_Memory_f616a41f5f598297276e1c7ec3a0c216}%

%{Phalcon_Cache_Backend_Memory_132cc48b9d06f81662724fafbcf5388d}%

%{Phalcon_Cache_Backend_Memory_25608a5b33db734ab297a041c17d0afe}%

%{Phalcon_Cache_Backend_Memory_4f309f2cbbf78c9cc272007b4b90e759}%

%{Phalcon_Cache_Backend_Memory_d95154d7678637473d5bd3651a6b7981}%

%{Phalcon_Cache_Backend_Memory_63ef827e599ff5e9ce28cac18e0a74de}%

%{Phalcon_Cache_Backend_Memory_8f54d16975ae8256cc1f314641e21364}%

%{Phalcon_Cache_Backend_Memory_7b222737002802a81ee0a69bdb497f3a}%

