%{Phalcon_Mvc_Model_MetaData_b664fd1d4a2fae794b28fda4e1d8aef0}%
================================================

%{Phalcon_Mvc_Model_MetaData_1082895e6fc81a382e87694939a07471|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Mvc\\Model\\MetaDataInterface <Phalcon_Mvc_Model_MetaDataInterface>`}%

%{Phalcon_Mvc_Model_MetaData_c236766f3ad36509d43ad3c063d5f7c4}%

.. code-block:: php

    <?php

    $metaData = new Phalcon\Mvc\Model\MetaData\Memory();
    $attributes = $metaData->getAttributes(new Robots());
    print_r($attributes);




%{Phalcon_Mvc_Model_MetaData_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Model_MetaData_ad74f2d914ee4a573eec52582659a420}%

%{Phalcon_Mvc_Model_MetaData_e0abd1b7adcd4cc1057ed137c2f09a3e}%

%{Phalcon_Mvc_Model_MetaData_28b1a820027c8d5238f38ff11cc743a7}%

%{Phalcon_Mvc_Model_MetaData_3db843532681e0aad64637c2cc33a7d6}%

%{Phalcon_Mvc_Model_MetaData_176fe76a0e9dfb8cada40cf9d14c8146}%

%{Phalcon_Mvc_Model_MetaData_1a97eea73ea575ee39d6134df260ed6c}%

%{Phalcon_Mvc_Model_MetaData_0eac75a1c8f4702eba89299182a2b9ae}%

%{Phalcon_Mvc_Model_MetaData_f1ed350772a37234fb81e24ffeee6745}%

%{Phalcon_Mvc_Model_MetaData_1f4890c5be487acdc7064d301c39b7bc}%

%{Phalcon_Mvc_Model_MetaData_5157b25eb7b88c5de365e8ec9357f7a8}%

%{Phalcon_Mvc_Model_MetaData_3d051e46f6507a13a2a95dd7608ad974}%

%{Phalcon_Mvc_Model_MetaData_54a690e551309d1445f1ddcbd58e77d2}%

%{Phalcon_Mvc_Model_MetaData_cf3c58249188cda629b3e0c33cb9d1d4}%

%{Phalcon_Mvc_Model_MetaData_f8d0e8f928e50d247123550a6fd035ec}%

%{Phalcon_Mvc_Model_MetaData_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_MetaData_38bce48ac0a5599a2f9c808808ef00b7}%

%{Phalcon_Mvc_Model_MetaData_f116958721070e69de6fc08feebcbeb4}%

%{Phalcon_Mvc_Model_MetaData_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_MetaData_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Model_MetaData_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_MetaData_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Mvc_Model_MetaData_dc191dd4072c7cec0f6a1542f92eb93e|:doc:`Phalcon\\Mvc\\Model\\MetaData\\Strategy\\Introspection <Phalcon_Mvc_Model_MetaData_Strategy_Introspection>`}%

%{Phalcon_Mvc_Model_MetaData_9f44824bd88973bbaf2ad18a8fd175d0}%

%{Phalcon_Mvc_Model_MetaData_229210b4ebd4281e29106506dddb4eda|:doc:`Phalcon\\Mvc\\Model\\MetaData\\Strategy\\Introspection <Phalcon_Mvc_Model_MetaData_Strategy_Introspection>`}%

%{Phalcon_Mvc_Model_MetaData_f3cf9b9c53d8dda002b7d1f6b19a6fd0}%

%{Phalcon_Mvc_Model_MetaData_5e431beac02a5f77595da9b3c22b77e4|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_04ab3007cd49e331a1f1693f71b788ac}%

.. code-block:: php

    <?php

    print_r($metaData->readMetaData(new Robots()));





%{Phalcon_Mvc_Model_MetaData_9bb2f03e68dac631d727a2176bd4df4a|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_e5d94be7443e2f236056aa4f381b0f37}%

.. code-block:: php

    <?php

    print_r($metaData->writeColumnMapIndex(new Robots(), MetaData::MODELS_REVERSE_COLUMN_MAP, array('leName' => 'name')));





%{Phalcon_Mvc_Model_MetaData_6dd6cf0743ad31fb8f8ba75df010d606|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_e3314861684e59667de64a6cbf16388a}%

.. code-block:: php

    <?php

    print_r($metaData->writeColumnMapIndex(new Robots(), MetaData::MODELS_REVERSE_COLUMN_MAP, array('leName' => 'name')));





%{Phalcon_Mvc_Model_MetaData_20deb3635e8de04ea63b91b5a237886a|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_6c469653b533736665a502e3ac99c0f2}%

.. code-block:: php

    <?php

    print_r($metaData->readColumnMap(new Robots()));





%{Phalcon_Mvc_Model_MetaData_6b0eb7a55b67b76c59d3f8818b97572e|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_6bc29a64f51f3b01f6e09945094c0244}%

.. code-block:: php

    <?php

    print_r($metaData->readColumnMapIndex(new Robots(), MetaData::MODELS_REVERSE_COLUMN_MAP));





%{Phalcon_Mvc_Model_MetaData_073f8b5d195bb05aa550dc1b0e09d532|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_c1d0805362967869500f1a68a1e7a766}%

.. code-block:: php

    <?php

    print_r($metaData->getAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_56aa543379fe5797c68442afc4709dbe|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_e46d0b16f50db6ab0a82ce2c3409182b}%

.. code-block:: php

    <?php

    print_r($metaData->getPrimaryKeyAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_68453550989204cd2903e21c5334a197|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_783a49534b7a705cefa15dd43fe05687}%

.. code-block:: php

    <?php

    print_r($metaData->getNonPrimaryKeyAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_7eacc8107f446747fe98a90f65023d04|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_e128f6f6423fdd9dd0e0cec9a4dc42b9}%

.. code-block:: php

    <?php

    print_r($metaData->getNotNullAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_950d898ce6ae9665c66c7931019c5a1c|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_95d0e1d9d682e081c9761d44f389e786}%

.. code-block:: php

    <?php

    print_r($metaData->getDataTypes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_efcfb583d527231f2cbbb68ea4da3395|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_cf9b62f97fa668ee794b0fdf95c90911}%

.. code-block:: php

    <?php

    print_r($metaData->getDataTypesNumeric(new Robots()));





%{Phalcon_Mvc_Model_MetaData_9e0137520705d98518e666e267d63b8d|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_2577ec492ffa5dabeae9628446e067cf}%

.. code-block:: php

    <?php

    print_r($metaData->getIdentityField(new Robots()));





%{Phalcon_Mvc_Model_MetaData_35403eb3331ee0a60550e2bd39abfcd1|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_311436393b211aa970fa1a73ce6e0073}%

.. code-block:: php

    <?php

    print_r($metaData->getBindTypes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_78e09b3b6ae0286b1d9bdc94022dfbab|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_ed25be665841fe536b7769da498ab68a}%

.. code-block:: php

    <?php

    print_r($metaData->getAutomaticCreateAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_bebfd10c2148793150e8c18ece40d1bc|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_7f8b5c27d46336c0c7e0592b2ff85320}%

.. code-block:: php

    <?php

    print_r($metaData->getAutomaticUpdateAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_64943edb24d3ee9e2a57087d54e6b19d|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_28120b008f1551a44aa2b6a76d818ffb}%

.. code-block:: php

    <?php

    $metaData->setAutomaticCreateAttributes(new Robots(), array('created_at' => true));





%{Phalcon_Mvc_Model_MetaData_f9ca4f15150701529dfce2681b95b8f4|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_376dcecabec9d3b30745082e3be09c88}%

.. code-block:: php

    <?php

    $metaData->setAutomaticUpdateAttributes(new Robots(), array('modified_at' => true));





%{Phalcon_Mvc_Model_MetaData_1b3fc33f67d5c4f2a86d1eabafdc7daf|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_308d8e4032f381676635193d983802a0}%

.. code-block:: php

    <?php

    print_r($metaData->getColumnMap(new Robots()));





%{Phalcon_Mvc_Model_MetaData_e2c0299c687bea2a4b623f08252a64e3|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_320eda8c5c16b03a7f7d581fc9501921}%

.. code-block:: php

    <?php

    print_r($metaData->getReverseColumnMap(new Robots()));





%{Phalcon_Mvc_Model_MetaData_9a9253bd22ebf8f2a4ce4bb9c0db7590|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_cdc7683d6bfc852fd0564914abd270be}%

.. code-block:: php

    <?php

    var_dump($metaData->hasAttribute(new Robots(), 'name'));





%{Phalcon_Mvc_Model_MetaData_bfc7da1cecff90e5f3f1f6b35eab6895}%

%{Phalcon_Mvc_Model_MetaData_dbb247008c6c3f3cd3b03f52e537ed38}%

.. code-block:: php

    <?php

    var_dump($metaData->isEmpty());





%{Phalcon_Mvc_Model_MetaData_439af7c78dd5d7550c7639ff961b7d62}%

%{Phalcon_Mvc_Model_MetaData_1b8177ab04242d263b23cf468916b044}%

.. code-block:: php

    <?php

    $metaData->reset();





%{Phalcon_Mvc_Model_MetaData_1de10f44c0482b8f01503a6a88ed2096}%

%{Phalcon_Mvc_Model_MetaData_c64c81d7d3cd6632b45912160b22a9a0}%

%{Phalcon_Mvc_Model_MetaData_23269d70afcbc578960e8521625a12ad}%

%{Phalcon_Mvc_Model_MetaData_52e7b16005a7a5b59aba9db1dd92208e}%

