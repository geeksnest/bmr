%{Phalcon_Mvc_Micro_Collection_3f0fc9c893e9766210f4563e00ecb5fd}%
=========================================

%{Phalcon_Mvc_Micro_Collection_d99638fc73e64a269a458ed7d205005a|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_e8998f86560ee58861b3db0dcd40b4dc}%

.. code-block:: php

    <?php

     $app = new Phalcon\Mvc\Micro();
    
     $collection = new Phalcon\Mvc\Micro\Collection();
    
     $collection->setHandler(new PostsController());
    
     $collection->get('/posts/edit/{id}', 'edit');
    
     $app->mount($collection);




%{Phalcon_Mvc_Micro_Collection_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Micro_Collection_2ba8dd42c447ae8d52432869515c1b2a|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_144c3079823a9678c95fe777710329b3}%

%{Phalcon_Mvc_Micro_Collection_6b0eaf94d5d7e5c946d8a4a2d0be788f}%

%{Phalcon_Mvc_Micro_Collection_2526186c93b55adacc235b06a6720451}%

%{Phalcon_Mvc_Micro_Collection_82330635ad623454f51a0dbc0d4834ca}%

%{Phalcon_Mvc_Micro_Collection_fbdeae23465e6e2ba387619a14dc308f}%

%{Phalcon_Mvc_Micro_Collection_73cb43f34b8f1ae88dea62ebc86b6794|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_6b79724bc71240e2a4f21fcc14030699}%

%{Phalcon_Mvc_Micro_Collection_b6f32099bf830d084b14f587dcec8a77|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_e7d21f5037b10b01b7078a2b2c6eb2ca}%

%{Phalcon_Mvc_Micro_Collection_8120c37a0506147b729ce9c5c131c79b}%

%{Phalcon_Mvc_Micro_Collection_33f109ae78863d18f8edc1647579c1de}%

%{Phalcon_Mvc_Micro_Collection_12dd01095cb019f6b827f24c58427744}%

%{Phalcon_Mvc_Micro_Collection_caabf5f8071b545c06ec155ca06d4c6e}%

%{Phalcon_Mvc_Micro_Collection_9b9d98cff6d536672368ae2cbe69fda3|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_4ef9782480eace7827044da4cc1f62a2}%

%{Phalcon_Mvc_Micro_Collection_035ac3e9e6d07130b65cd60d9d797c92|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_f6de54e51c2d392ef831a96d9b79e021}%

%{Phalcon_Mvc_Micro_Collection_64ba53015d254ba5f8c72b75230d8e7e|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_0232581065af0bae3a056c661f259e7b}%

%{Phalcon_Mvc_Micro_Collection_7fdc89cc46253f6971d13212ca0470fd|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_40d7f27c4f232dfdabda1507adb45f0c}%

%{Phalcon_Mvc_Micro_Collection_2d06d59bb518632c0b3d8daccabf125f|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_11e7efdfc40c792183ea3a0a25bc2b1a}%

%{Phalcon_Mvc_Micro_Collection_6d296530dc9deab6285211a62e0f464a|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_9ff9d0f097137fe71c60c343c639f362}%

%{Phalcon_Mvc_Micro_Collection_badbb766a483c568d8b3a05b2844e8af|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_9ce8e9cfdc4f1e99014a11a1c9ba5708}%

%{Phalcon_Mvc_Micro_Collection_07a418565d64cf1dc461d40825889376|:doc:`Phalcon\\Mvc\\Micro\\CollectionInterface <Phalcon_Mvc_Micro_CollectionInterface>`}%

%{Phalcon_Mvc_Micro_Collection_a674cd33cea531814c921719a89fe67b}%

