%{Phalcon_Validation_Validator_Digit_516327547abced1d58d889276b9a20db}%
===============================================

%{Phalcon_Validation_Validator_Digit_24302afb0327181552d5097efe21d21a|:doc:`Phalcon\\Validation\\Validator <Phalcon_Validation_Validator>`}%

%{Phalcon_Validation_Validator_Digit_3bc33f5008c906bbf75c850085f3e576|:doc:`Phalcon\\Validation\\ValidatorInterface <Phalcon_Validation_ValidatorInterface>`}%

%{Phalcon_Validation_Validator_Digit_a0a5331f9203c30c7f3369fee7abd8df}%

.. code-block:: php

    <?php

    use Phalcon\Validation\Validator\Digit as DigitValidator;
    
    $validator->add('height', new DigitValidator(array(
       'message' => ':field must be numeric'
    )));




%{Phalcon_Validation_Validator_Digit_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Validator_Digit_9a9ac34e57881f672b6419517f5ca372}%

%{Phalcon_Validation_Validator_Digit_d01ba9899fa892ec2b62154529dce37f}%

%{Phalcon_Validation_Validator_Digit_5009115a2e23762a2cebed7a090308f5}%

%{Phalcon_Validation_Validator_Digit_ec865eb22d34722d5b3dfe1c2403678d}%

%{Phalcon_Validation_Validator_Digit_fbdf133d897ae25a2586e5ee3f08ccce}%

%{Phalcon_Validation_Validator_Digit_13f58da2ee2f606006004d67f4b648cc}%

%{Phalcon_Validation_Validator_Digit_08d6177f98e66f04346768bf61e2e6ca}%

%{Phalcon_Validation_Validator_Digit_f2c1e7b80644ef8d34a17b0c2a367fff}%

%{Phalcon_Validation_Validator_Digit_ba312e603be421ffda5e8b7ddba88d42}%

%{Phalcon_Validation_Validator_Digit_2e7772bfbad31074c960e8f21b33e650}%

