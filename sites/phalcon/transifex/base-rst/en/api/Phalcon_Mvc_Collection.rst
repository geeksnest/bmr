%{Phalcon_Mvc_Collection_c5f6e310ee784c15dc372b7f2c52558e}%
==================================

%{Phalcon_Mvc_Collection_80ea4044991ed0bd372a175ac0c7446d|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Collection_a2c4a67ae3b75ced625c10f8259fb234}%

%{Phalcon_Mvc_Collection_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Collection_60fc354d019cc69a556db79952e9fe27}%

%{Phalcon_Mvc_Collection_65a0d3777c5a9c21e0a287986c8fc77c}%

%{Phalcon_Mvc_Collection_b4b6325a6a4b6248d3b7a4131c13b6bf}%

%{Phalcon_Mvc_Collection_55da66508c354dcc0293409f7bed55ef}%

%{Phalcon_Mvc_Collection_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Collection_794046e53a0eecb531ec997f254a6a77|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`|:doc:`Phalcon\\Mvc\\Collection\\ManagerInterface <Phalcon_Mvc_Collection_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_c3c0ba014162bf7967f764fab4b191c3}%

%{Phalcon_Mvc_Collection_2ad0364e3872c0405e2fd97fd0679fa8}%

%{Phalcon_Mvc_Collection_d3fdac985cbded10a662faf71feb5433}%

%{Phalcon_Mvc_Collection_7196d4953de5df3b9ef11aab0594501f}%

%{Phalcon_Mvc_Collection_a233716fa015c70b44f8bda5b25b761c}%

%{Phalcon_Mvc_Collection_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Collection_992eb21f94076f486939b432849d799c}%

%{Phalcon_Mvc_Collection_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Collection_1bac54934bec20940b708d05267cfd49}%

%{Phalcon_Mvc_Collection_9d238d84822d19ffba2c187e8f97af84|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_5abe60a2267d1a558edff05114e36ed6}%

%{Phalcon_Mvc_Collection_83e406da5dbad46d8845e952a7c95908|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_3e152c1d4a8e1e44eb8e734d54a06599}%

%{Phalcon_Mvc_Collection_1aba208fb458006629695c191000d162|:doc:`Phalcon\\Mvc\\Model\\ManagerInterface <Phalcon_Mvc_Model_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_cc64153fcb9f9eef892abdff0655326e}%

%{Phalcon_Mvc_Collection_79cd66b859291ff1938788c11c199916}%

%{Phalcon_Mvc_Collection_fefede2f87f17aa4b0bc75f8a6f905ab}%

%{Phalcon_Mvc_Collection_c89813cfab54fa1c2ab41870e6db4fbd}%

%{Phalcon_Mvc_Collection_7c8f580e0d9b3029bc0066d6c2445f51}%

%{Phalcon_Mvc_Collection_ed307fcf1dfb8a08ef907794285cce9e|:doc:`Phalcon\\Mvc\\Collection <Phalcon_Mvc_Collection>`}%

%{Phalcon_Mvc_Collection_8836620c34757c39e806e5eaa4ee3acb}%

%{Phalcon_Mvc_Collection_926f986e31ff4993ee80a37f1bdb73fc}%

%{Phalcon_Mvc_Collection_e4b53ce18b287a30ab15bb6065ec15cd}%

%{Phalcon_Mvc_Collection_e388410b93813848c99ab5607f48c81c|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Collection_bb876477e44b94495f4355fbb2463a34}%

%{Phalcon_Mvc_Collection_7f9eed198d356a07a3bad6b9b3bde18f}%

%{Phalcon_Mvc_Collection_7924c1ce9c03f803cea438d23ebaeb94}%

%{Phalcon_Mvc_Collection_3bc5aa412e6c669e22875a7d98e665ac}%

%{Phalcon_Mvc_Collection_1a6ab3e9f222a29296a1e7f8f41639bd}%

%{Phalcon_Mvc_Collection_7b793f1f8fc0c0c603c6f68d21385cde}%

%{Phalcon_Mvc_Collection_541ca63c8f0e9cdb167f3c5dff81b126}%

.. code-block:: php

    <?php

    echo $robot->readAttribute('name');





%{Phalcon_Mvc_Collection_5edaecf6057a10ae28a47003da1f9669}%

%{Phalcon_Mvc_Collection_7c2a8da2a9aeb392f8b2784da24c73a8}%

.. code-block:: php

    <?php

    $robot->writeAttribute('name', 'Rosey');





%{Phalcon_Mvc_Collection_589614cf8b9e5a27db690cd3a193fad7|:doc:`Phalcon\\Mvc\\Collection <Phalcon_Mvc_Collection>`|:doc:`Phalcon\\Mvc\\Collection <Phalcon_Mvc_Collection>`}%

%{Phalcon_Mvc_Collection_eb7e1877b1643437701c66bd54c5771c}%

%{Phalcon_Mvc_Collection_932f6a10d1878b5051791324f27aa221}%

%{Phalcon_Mvc_Collection_96e8ea1e10ffc2fc044009c103b56ecc}%

%{Phalcon_Mvc_Collection_87a83f05a7285b8abe58a4daff914a9c}%

%{Phalcon_Mvc_Collection_9c1aaa840daf5d6e66fb2d1f32f43fdb}%

%{Phalcon_Mvc_Collection_a67a23c55246f25f341fbca38d4b4b46}%

%{Phalcon_Mvc_Collection_cfbd41b594299a628d4f66287116c627}%

%{Phalcon_Mvc_Collection_4ff343e39a492da6de2ff25afaf35a97}%

%{Phalcon_Mvc_Collection_6a15eae29ad184484aa2c5f0033edf60}%

%{Phalcon_Mvc_Collection_62a2411be88b1b5c3ba5fd4b140a318e}%

%{Phalcon_Mvc_Collection_6d51137d07f724c3dcf00e535777fb51}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Validator\ExclusionIn as ExclusionIn;
    
    class Subscriptors extends Phalcon\Mvc\Collection
    {
    
    public function validation()
    {
    	$this->validate(new ExclusionIn(array(
    		'field' => 'status',
    		'domain' => array('A', 'I')
    	)));
    	if ($this->validationHasFailed() == true) {
    		return false;
    	}
    }
    
    }





%{Phalcon_Mvc_Collection_5fbc57c37116623f09aefb62a97fdb0a}%

%{Phalcon_Mvc_Collection_b62a0696d8ccc76212fbf8e99273f8df}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Validator\ExclusionIn as ExclusionIn;
    
    class Subscriptors extends Phalcon\Mvc\Collection
    {
    
    public function validation()
    {
    	$this->validate(new ExclusionIn(array(
    		'field' => 'status',
    		'domain' => array('A', 'I')
    	)));
    	if ($this->validationHasFailed() == true) {
    		return false;
    	}
    }
    
    }





%{Phalcon_Mvc_Collection_f47d0f98b19e11f53daa67eff0ab6490}%

%{Phalcon_Mvc_Collection_84197a302bd65ca475ca8185f48ff975}%

%{Phalcon_Mvc_Collection_efc588f4ffbf7acb46db3cebddac394c}%

%{Phalcon_Mvc_Collection_69f2ac40d5ee65f0ef6fad1b7a03be72}%

%{Phalcon_Mvc_Collection_fbf0399ebfc7f8cff11f317323eb35e9}%

%{Phalcon_Mvc_Collection_81763188c0e83ea527b222f05affffc8}%

%{Phalcon_Mvc_Collection_aef96aa1fa2aedc2a339f65b7f11607a}%

%{Phalcon_Mvc_Collection_1eba869a02eb8c4a0f7e4305681bb974}%

%{Phalcon_Mvc_Collection_0275cb947f8840a688e0d40528dc8dcd|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Collection_5577872af356ca5ceebe7cff99019cae}%

.. code-block:: php

    <?php

    $robot = new Robots();
    $robot->type = 'mechanical';
    $robot->name = 'Astro Boy';
    $robot->year = 1952;
    if ($robot->save() == false) {
    echo "Umh, We can't store robots right now ";
    foreach ($robot->getMessages() as $message) {
    	echo $message;
    }
    } else {
    echo "Great, a new robot was saved successfully!";
    }





%{Phalcon_Mvc_Collection_de7efd470219dcf461348cf047e2fddb|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Collection_0957aac3d3f7b9ddef640d8851cd3376}%

.. code-block:: php

    <?php

    use \Phalcon\Mvc\Model\Message as Message;
    
    class Robots extends Phalcon\Mvc\Model
    {
    
    	public function beforeSave()
    	{
    		if ($this->name == 'Peter') {
    			$message = new Message("Sorry, but a robot cannot be named Peter");
    			$this->appendMessage($message);
    		}
    	}
    }





%{Phalcon_Mvc_Collection_20aac02ad9edc772b5112a8b0a91a143}%

%{Phalcon_Mvc_Collection_64779f3e875fc210fd37e6b841ea73e2}%

%{Phalcon_Mvc_Collection_ceae11ee0d0494e096eefd4a3ad76f93|:doc:`Phalcon\\Mvc\\Collection <Phalcon_Mvc_Collection>`}%

%{Phalcon_Mvc_Collection_d2cfd6f79346430859011dc014d8d365}%

%{Phalcon_Mvc_Collection_864caf962891bafed70e0e0a03e45191}%

%{Phalcon_Mvc_Collection_1c5c91736334264f6a703c4c67c49b88}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Collection_d0e84f56294aba5d7c361d5aafea2a31%}
     $robot = Robots::findFirst();
     echo "The robot name is ", $robot->name, "\n";
    
     //{%Phalcon_Mvc_Collection_fa26e0247c99764efc46dcd460d4ecdd%}
     $robot = Robots::findFirst(array(
         array("type" => "mechanical")
     ));
     echo "The first mechanical robot name is ", $robot->name, "\n";
    
     //{%Phalcon_Mvc_Collection_d3f491a4553e16e050d7435bc9820fba%}
     $robot = Robots::findFirst(array(
         array("type" => "mechanical"),
         "sort" => array("name" => 1)
     ));
     echo "The first virtual robot name is ", $robot->name, "\n";





%{Phalcon_Mvc_Collection_9de9f265415c5ee2473139968657cf73}%

%{Phalcon_Mvc_Collection_ab1c037781388fce804cd04c96a0bd41}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Collection_1499c1af63a87b3cd78713aeabe53fc5%}
     $robots = Robots::find();
     echo "There are ", count($robots), "\n";
    
     //{%Phalcon_Mvc_Collection_87078387b9e7d1df974b6134db85d304%}
     $robots = Robots::find(array(
         array("type" => "mechanical")
     ));
     echo "There are ", count($robots), "\n";
    
     //{%Phalcon_Mvc_Collection_8565a9d6967f26ce0d27ddb21a21ea4e%}
     $robots = Robots::findFirst(array(
         array("type" => "virtual"),
         "sort" => array("name" => 1)
     ));
     foreach ($robots as $robot) {
       echo $robot->name, "\n";
     }
    
     //{%Phalcon_Mvc_Collection_815bac1c09b347d15b0d2a88e4b3cacd%}
     $robots = Robots::find(array(
         array("type" => "virtual"),
         "sort" => array("name" => 1),
         "limit" => 100
     ));
     foreach ($robots as $robot) {
       echo $robot->name, "\n";
     }





%{Phalcon_Mvc_Collection_06a40fac9b9ede0f3c5cc936cfb1ab80}%

%{Phalcon_Mvc_Collection_30c3e6d7c06c84b81c83f0e0a1433eff}%

.. code-block:: php

    <?php

     echo 'There are ', Robots::count(), ' robots';





%{Phalcon_Mvc_Collection_7431ea5d95ba1a90298e643e42dbd819}%

%{Phalcon_Mvc_Collection_0f7c4a68b7d59ad1717eb46a1cb3672d}%

%{Phalcon_Mvc_Collection_c6343f26c44547efc18b2bc1bc4f807e}%

%{Phalcon_Mvc_Collection_226cdf84e2fc7c4d38b8f50ec2653d5c}%

%{Phalcon_Mvc_Collection_6bc264b1ff9d7da0a92aba43b7434fc8}%

%{Phalcon_Mvc_Collection_69fa51fa19e89c90ab5a559889604610}%

.. code-block:: php

    <?php

    $robot = Robots::findFirst();
    $robot->delete();
    
    foreach (Robots::find() as $robot) {
    	$robot->delete();
    }





%{Phalcon_Mvc_Collection_94cda9aff69544f071844a8c2e06c127}%

%{Phalcon_Mvc_Collection_aee061a540ff24e6f0999f437c33de16}%

.. code-block:: php

    <?php

     print_r($robot->toArray());





%{Phalcon_Mvc_Collection_e4a1eb998f48d9e9518e537dd9bf4090}%

%{Phalcon_Mvc_Collection_7104cd41d6c531eea3d4f37c85ab4149}%

%{Phalcon_Mvc_Collection_e299d36d06648957330fcf22840f28f9}%

%{Phalcon_Mvc_Collection_5712a0fc046b2823070d4e91fff6bbbb}%

%{Phalcon_Mvc_Collection_48fb91d7a402869be0194d0c9273c01b}%

%{Phalcon_Mvc_Collection_746dbd17677479ad970d8e738d30e158}%

