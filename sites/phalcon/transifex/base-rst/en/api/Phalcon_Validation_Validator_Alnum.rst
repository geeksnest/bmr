%{Phalcon_Validation_Validator_Alnum_abd1da828229bfd8bc423140f75e583f}%
===============================================

%{Phalcon_Validation_Validator_Alnum_24302afb0327181552d5097efe21d21a|:doc:`Phalcon\\Validation\\Validator <Phalcon_Validation_Validator>`}%

%{Phalcon_Validation_Validator_Alnum_3bc33f5008c906bbf75c850085f3e576|:doc:`Phalcon\\Validation\\ValidatorInterface <Phalcon_Validation_ValidatorInterface>`}%

%{Phalcon_Validation_Validator_Alnum_cf273a03a6159b7c35e15dd3cac491fe}%

.. code-block:: php

    <?php

    use Phalcon\Validation\Validator\Alnum as AlnumValidator;
    
    $validator->add('username', new AlnumValidator(array(
       'message' => ':field must contain only alphanumeric characters'
    )));




%{Phalcon_Validation_Validator_Alnum_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Validator_Alnum_9a9ac34e57881f672b6419517f5ca372}%

%{Phalcon_Validation_Validator_Alnum_d01ba9899fa892ec2b62154529dce37f}%

%{Phalcon_Validation_Validator_Alnum_5009115a2e23762a2cebed7a090308f5}%

%{Phalcon_Validation_Validator_Alnum_ec865eb22d34722d5b3dfe1c2403678d}%

%{Phalcon_Validation_Validator_Alnum_fbdf133d897ae25a2586e5ee3f08ccce}%

%{Phalcon_Validation_Validator_Alnum_13f58da2ee2f606006004d67f4b648cc}%

%{Phalcon_Validation_Validator_Alnum_08d6177f98e66f04346768bf61e2e6ca}%

%{Phalcon_Validation_Validator_Alnum_f2c1e7b80644ef8d34a17b0c2a367fff}%

%{Phalcon_Validation_Validator_Alnum_ba312e603be421ffda5e8b7ddba88d42}%

%{Phalcon_Validation_Validator_Alnum_2e7772bfbad31074c960e8f21b33e650}%

