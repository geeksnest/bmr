%{Phalcon_Mvc_Model_TransactionInterface_028f4a021d6d87182a1548d5ccb9befa}%
=======================================================

%{Phalcon_Mvc_Model_TransactionInterface_0b53b764c307a019af49b648c5c13bff}%

%{Phalcon_Mvc_Model_TransactionInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_TransactionInterface_185610681c586f38ac00c8ec6742af36|:doc:`Phalcon\\Mvc\\Model\\Transaction\\ManagerInterface <Phalcon_Mvc_Model_Transaction_ManagerInterface>`}%

%{Phalcon_Mvc_Model_TransactionInterface_ea3db93b456728d20e714298e4ca3159}%

%{Phalcon_Mvc_Model_TransactionInterface_701eb56e53484b6359cef689bf840681}%

%{Phalcon_Mvc_Model_TransactionInterface_bff79e282b8671b90a6ad594e67c144b}%

%{Phalcon_Mvc_Model_TransactionInterface_d1a0fbd17efeda9b70915c57c5a8a20c}%

%{Phalcon_Mvc_Model_TransactionInterface_e056771a69c4eda4f4ad66240e2955eb}%

%{Phalcon_Mvc_Model_TransactionInterface_0a1d1f8b4f8f312ac187a72807401c03|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_TransactionInterface_5dc045936950815b201ec1c8c801de14}%

%{Phalcon_Mvc_Model_TransactionInterface_6d2f3f6bb3e187b354ad29d433cc38ec}%

%{Phalcon_Mvc_Model_TransactionInterface_d1e95f87797d745956c9ae84fcc3e9fc}%

%{Phalcon_Mvc_Model_TransactionInterface_fc0e11ba3d3a631ee7b71cefb6937cea}%

%{Phalcon_Mvc_Model_TransactionInterface_f5881204525b0ebff024cccca2ba1d51}%

%{Phalcon_Mvc_Model_TransactionInterface_4bddea27230995fc8337a88301723793}%

%{Phalcon_Mvc_Model_TransactionInterface_200807cd22934c89548f0f79dadd1b39}%

%{Phalcon_Mvc_Model_TransactionInterface_c27656acb621daea22a7bbb08d328555}%

%{Phalcon_Mvc_Model_TransactionInterface_65519ff7fb0a0fdbdeb36c7ac2eeaff1}%

%{Phalcon_Mvc_Model_TransactionInterface_507f35aab1534416c9d7071e61021720}%

%{Phalcon_Mvc_Model_TransactionInterface_980e5df422d01dc3b06f345648706351}%

%{Phalcon_Mvc_Model_TransactionInterface_783d5b1373df4f1b2af7944a2186b5fc}%

%{Phalcon_Mvc_Model_TransactionInterface_8078cf850889cf9d7414d074f298bc8f}%

%{Phalcon_Mvc_Model_TransactionInterface_60c8068181d8098c2dd8ce2c9679a1ba|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_TransactionInterface_5cf15634e6ff82de122f69c1038bc677}%

