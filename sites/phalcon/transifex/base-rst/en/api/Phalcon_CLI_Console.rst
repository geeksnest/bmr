%{Phalcon_CLI_Console_d001116cc4b4f89361601a528028af87}%
===============================

%{Phalcon_CLI_Console_25154160f51d01df0ffad59584cfed99|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`}%

%{Phalcon_CLI_Console_6636a700681cd831645eedff7e0e3607}%

%{Phalcon_CLI_Console_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_CLI_Console_7189f8671d75aa5689bd46a2e118ef3b}%

%{Phalcon_CLI_Console_f114e82aa6bc9e734ec30fecefef097c}%

%{Phalcon_CLI_Console_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Console_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_CLI_Console_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Console_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_CLI_Console_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_CLI_Console_8d848637cfda8b4cc05eedcec9549316}%

%{Phalcon_CLI_Console_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_CLI_Console_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_CLI_Console_4f146bbf144a68ef0ef390b6213b27df}%

%{Phalcon_CLI_Console_79cf35523cdd6d49632d38284ae3cfae}%

.. code-block:: php

    <?php

    $application->registerModules(array(
    	'frontend' => array(
    		'className' => 'Multiple\Frontend\Module',
    		'path' => '../apps/frontend/Module.php'
    	),
    	'backend' => array(
    		'className' => 'Multiple\Backend\Module',
    		'path' => '../apps/backend/Module.php'
    	)
    ));





%{Phalcon_CLI_Console_bd8720d15fb4b6b51e75c97253fc8987}%

%{Phalcon_CLI_Console_386c942a43739f1d47b8b01ac6612c51}%

.. code-block:: php

    <?php

    $application->addModules(array(
    	'admin' => array(
    		'className' => 'Multiple\Admin\Module',
    		'path' => '../apps/admin/Module.php'
    	)
    ));





%{Phalcon_CLI_Console_2b65900c295fb31cc1c4d306b08bd796}%

%{Phalcon_CLI_Console_c78e4e5fd4c032e1946c0281aa60d692}%

%{Phalcon_CLI_Console_95b574f20b07eae1c0f6ee7d905fedf1}%

%{Phalcon_CLI_Console_568d7bc65c6f229ca13aec35c856d0c1}%

