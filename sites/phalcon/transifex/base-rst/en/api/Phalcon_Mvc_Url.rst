%{Phalcon_Mvc_Url_87fd9319f1246ec1bdf01f3649522638}%
===========================

%{Phalcon_Mvc_Url_8ecae32ff999900259c2329c10fbc6dd|:doc:`Phalcon\\Mvc\\UrlInterface <Phalcon_Mvc_UrlInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Url_ef76993a2c3f92abdd87a801102a1bc7}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Url_fbb51cdfcc9d881792e26547c2a256e3%}
     echo $url->get('products/edit/1');
    
     //{%Phalcon_Mvc_Url_66a526ad3f62c2661a2173bec3be17f9%}
     echo $url->get(array('for' => 'blog-post', 'title' => 'some-cool-stuff', 'year' => '2012'));




%{Phalcon_Mvc_Url_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Url_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Url_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Url_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Url_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Mvc_Url_d9b5653f970e21b4383d20568342c5e8|:doc:`Phalcon\\Mvc\\Url <Phalcon_Mvc_Url>`}%

%{Phalcon_Mvc_Url_cf860c8741c7917fca893302681b9d30}%

.. code-block:: php

    <?php

    $url->setBaseUri('/invo/');
    $url->setBaseUri('/invo/index.php/');





%{Phalcon_Mvc_Url_9805ca4626a90f756a9bcee0a3501027|:doc:`Phalcon\\Mvc\\Url <Phalcon_Mvc_Url>`}%

%{Phalcon_Mvc_Url_e08485966749f5fbd7322e7688a0a6ae}%

.. code-block:: php

    <?php

    $url->setStaticBaseUri('/invo/');





%{Phalcon_Mvc_Url_6a90afbfa7ea756f8e59a7795d788d91}%

%{Phalcon_Mvc_Url_03b8fe1207eee87d02fa3b9f892990a9}%

%{Phalcon_Mvc_Url_127d0d0aa8dcd4a564db68e8a006cc61}%

%{Phalcon_Mvc_Url_9121001f6db1644f8fa6d87cbd571956}%

%{Phalcon_Mvc_Url_1f6991fe58227abeb488ec0761e65233|:doc:`Phalcon\\Mvc\\Url <Phalcon_Mvc_Url>`}%

%{Phalcon_Mvc_Url_c569c6bf590d3dae99f051d99d40aa89}%

.. code-block:: php

    <?php

    $url->setBasePath('/var/www/htdocs/');





%{Phalcon_Mvc_Url_62ea62c61198412e434030ee2f11c7e1}%

%{Phalcon_Mvc_Url_244ba540ddeedb3b57625050303e39a7}%

%{Phalcon_Mvc_Url_c68fb47c9ce94211f9e3e30c8401e2bf}%

%{Phalcon_Mvc_Url_ff81652a8d797fc9159551c39f979401}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Url_fbb51cdfcc9d881792e26547c2a256e3%}
     echo $url->get('products/edit/1');
    
     //{%Phalcon_Mvc_Url_66a526ad3f62c2661a2173bec3be17f9%}
     echo $url->get(array('for' => 'blog-post', 'title' => 'some-cool-stuff', 'year' => '2012'));





%{Phalcon_Mvc_Url_ae86c4bffd5727172dca8f86138d7b8c}%

%{Phalcon_Mvc_Url_e36d7ec33993f7e606cdb77b3c9fffa5}%

%{Phalcon_Mvc_Url_936a09fb5e941eaa4e81476428264ce7}%

%{Phalcon_Mvc_Url_bb316bdcfb4440fa933a01f7119ece2e}%

