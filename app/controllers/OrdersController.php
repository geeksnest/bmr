<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class OrdersController extends Controller
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}

	public function indexAction()
	{
		$this->view->setVar('header_title', "Orders");
		$date= date("y-m-d"); 
		///////////////////////////////////////////////////////
		$currentsetdate=Setpricedate::find();/////////////////////////////////////
		foreach ($currentsetdate as $price) {
			$curdate =  $price->date;
			
		}
		$sands = Sand::find(array("date='".$curdate."'"));
		foreach ($sands as $m) {
			$data_sands[] = array(
				"id" => $m->id ,
				"sandtype" => $m->sandtype ,
				"sandcateg" => $m->sandcateg,
				"price" => $m->price
				);
		}
		////////////////////////////////////////////////////////



		$this->view->sandsdata = json_decode(json_encode($data_sands)); //LIST SAND CATEGORY
		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
					);
			}
			return json_decode(json_encode($data));
		}
		$this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
	 	$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY
	 	//////////////////////////////////////////////////////SAVE DATA
	 	if ($this->request->isPost('save') == true) {
	 		////VARIABLES
	 		$plateno= $this->request->getPost('plate');
	 		$truck = Trucks::find(array("plateno='".$plateno."'"));
	 		$comp= Company::find(array("id='".$truck[0]->company."'"));
	 		$company=$comp[0]->id;

	 		$sand= $this->request->getPost('sand') ;
	 		$cubmtr= $this->request->getPost('cubmtr') ;

	 		$po="";
	 		$cash="";

	 		$opt =$this->request->getPost('payopt');
	 		if($opt==0){
	 			$cash= $this->request->getPost('amount') ;
	 			$po="";

	 		}else{
	 			$po= $this->request->getPost('amount');
	 			$cash="";
	 		}

	 		$sandprice= $this->request->getPost('sandprice') ;
	 		$discount= $this->request->getPost('discount') ;
	 		$drno= $this->request->getPost('dr') ;
	 		$time= $this->request->getPost('time') ;
	 		



	 		//////Save ORDER DATE Ordereddate

	 		$ifexist =Ordereddate::find(array("date='".$date."'"));
	 		if(count($ifexist)==0){
	 			$savedate = new Ordereddate();
	 			$savedate->date = $date;
	 			if ($savedate ->save() == false) { 
	 				// echo "Umh, We can store data: ";
	 			} else {
	 				// echo "Great, a new data was saved successfully!";
	 			}
	 		}
	 		

	 		//////Save ORDER DATE
	 		/////////save ORDERS

	 		$add = new Orders();
	 		$add->plateno 	= $plateno;
	 		$add->company 	= $company;
	 		$add->sand 	= $sand;
	 		$add->cumtr 	=$cubmtr ;
	 		$add->pricecumtr 	= " ";
	 		$add->cash 	= $cash ;
	 		$add->poamount 	= $po;
	 		$add->discount 	= $discount;
	 		$add->drno 	= $drno;
	 		$add->time 	= $time;
	 		$add->date 	= $date;

	 		if ($add->save() == false) { 
	 			echo "Umh, We can store data: ";
	 		} else {
	 			echo "Great, a new data was saved successfully!";
	 		}

	 		/////////save ORDERS
	 	}


	 	function displaycateg($model,$column){
	 		$sand = $model::find();
	 		$data = array();
	 		foreach ($sand as $m) {
	 			$data[] = array(
	 				'id' => $m->id ,
	 				$column => $m->$column,
	 				);
	 		}
	 		return json_decode(json_encode($data));
	 	}

	 	function getsand(){
	 		$sand = Sand::find();
	 		$data = array();
	 		foreach ($sand as $m) {
	 			$data[] = array(
	 				'id' => $m->id ,
	 				"sandcateg"=> $m->sandcateg
	 				);
	 		}
	 		return json_decode(json_encode($data));
	 	}
	 	function company(){
	 		$sand = Company::find();
	 		$data = array();
	 		foreach ($sand as $m) {
	 			$data[] = array(
	 				'id' => $m->id ,
	 				"campname"=> $m->campname
	 				);
	 		}
	 		return json_decode(json_encode($data));
	 	}

		$this->view->data_categ = displaycateg("Sandcateg","category"); //LIST SAND CATEGORY
		$this->view->getsandcateg = getsand(); //GET SAND CATEGORY
		$this->view->company = company(); //GET SAND CATEGORY


		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}
		// The data set to paginate
		$orders      = Orders::find(array("date='".$date."' ORDER BY time DESC"));

		// Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator   = new PaginatorModel(
			array(
				"data"  => $orders,
				"limit" =>50,
				"page"  => $currentPage
				)
			);

		// Get the paginated results
		$this->view->orders= $paginator->getPaginate();;


	}


}
