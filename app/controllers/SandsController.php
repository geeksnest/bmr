<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class SandsController extends Controller
{

	public function initialize(){
		$this->view->setTemplateAfter('template');
	}
	
	public function addsandAction()
	{	
		//VARIABLES
		$this->view->setVar('header_title', "Add New Sand");
		$this->view->setVar('notification', "");
		//ADD SAND TYPE AND CATEGORY FUNCTION 
		function add($model,$column,$data){
			$add = new $model();
			$add->$column 	= $data;

			if ($add->save() == false) { 
				return "Umh, We can store data: ";
			} else {
				return "Great, a new data was saved successfully!";
			}
		}
		//END ADD SAND TYPE AND CATEGORY FUNCTION 

		//DISPLAY FUNCTION
		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
					);
			}
			return json_decode(json_encode($data));
		}
		//END DISPLAY FUNCTION
		if ($this->request->isPost('savetype') == true){  add("Sandtype","type",$this->request->getPost('sandtype'));}; //ADD SAND TYPE
		if ($this->request->isPost('savecateg') == true){ add("Sandcateg","category",$this->request->getPost('sandcateg'));}; //ADD SAND CATEGORY
        $this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
		$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY

		///ADD NEW SAND
		$currentsetdate=Setpricedate::find();/////////////////////////////////////
			foreach ($currentsetdate as $price) {
				$curdate =  $price->date;
				
			}
		if ($this->request->isPost('savesand') == true){
			// $type= $this->request->getPost('type');
			$type= "";
			$categ= $this->request->getPost('categ') ;
			$price= $this->request->getPost('price') ;


			$add = new Sand();
			$add->sandtype 	= $type;
			$add->sandcateg = $categ;
			$add->price 	= $price;
			$add->date 		= $curdate;

			if ($add->save() == false) { 
				echo "Umh, We can store data: ";
			} else {
				echo "Great, a new data was saved successfully!";
			}


		}; //ADD SAND TYPE
	}
	public function sandListAction()
	{
		$this->view->setVar('header_title', "Product Sand List");


		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
					);
			}
			return json_decode(json_encode($data));
		}
		$this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
		$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY



		// Current page to show
		// In a controller this can be:
		// $this->request->getQuery('page', 'int'); // GET
		// $this->request->getPost('page', 'int'); // POST
		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}
		
		////////////////////////LIST PRODUCT
		$currentsetdate=Setpricedate::find();/////////////////////////////////////
		foreach ($currentsetdate as $price) {
			$curdate =  $price->date;
			
		}

		// The data set to paginate
		$sand      = Sand::find(array("date='".$curdate."'"));

		// Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator   = new PaginatorModel(
			array(
				"data"  => $sand,
				"limit" => 10,
				"page"  => $currentPage
				)
			);

		// Get the paginated results
		$this->view->page= $paginator->getPaginate();;
	}
	
	public function setpriceAction()
	{
		$this->view->setVar('header_title', "Set Product Price");


		///////////////////////////////////////////////////
		////////////////////////LIST PRODUCT
		$currentsetdate=Setpricedate::find();/////////////////////////////////////
		foreach ($currentsetdate as $price) {
			$curdate =  $price->date;
			
		}

		$sand      = Sand::find(array("date='".$curdate."'"));

		foreach ($sand as $m) {
			$data[] = array(
				'id' => $m->id ,
				'sandtype' => $m->sandtype,
				'sandcateg' => $m->sandcateg,
				'price' => $m->price,
				);
		}
		$this->view->sand= json_decode(json_encode($data)) ;


		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
					);
			}
			return json_decode(json_encode($data));
		}
		$this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
		$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY



		/////////////////////////////////////////////////SAVE
		if ($this->request->isPost('save') == true) {
			$checked_count = count($_POST['sandid']);
			$sandid = $_POST['sandid']; ////////data variable
			$type = $_POST['type'];////////data variable
			$categ = $_POST['categ'];////////data variable
			$price = $_POST['price'];////////data variable
			$date = date("y-m-d");	////////data variable

			//validate
			$checkexist= Setpricedate::find(array("date='".$date."'"));
			if(count($checkexist)==0){
				$savedate= new Setpricedate();
				$savedate->date = $date;
				if ($savedate->save() == false) { 
					echo "Umh, We can store data: ";
				} else {
					echo "Great, a new data was saved successfully!";
				}

			 	//////Add New Price
				for($i=0;$i<=$checked_count;$i++){

					$add = new Sand();
					$add->sandtype 	= @$type[$i];
					$add->sandcateg = @$categ[$i];
					$add->price 	= @$price[$i];
					$add->date 		= $date;
					if ($add->save() == false) { 
				 		// echo "Umh, We can store data: ";
					} else {
				 		// echo "Great, a new data was saved successfully!";
					}

				}
			 	//////////////////////////////////////////////////////////
			}else{
				$dlt = Sand::find(array("date='".$date."'"));
				$data = array('error' => 'Not Found');
				if ($dlt) {
					if($dlt->delete()){
						for($i=0;$i<=$checked_count;$i++){
							$add = new Sand();
							$add->sandtype 	= "";
							$add->sandcateg = @$categ[$i];
							$add->price 	= @$price[$i];
							$add->date 		= $date;
							if ($add->save() == false) { 
						 		  $data = "Umh, We can store data: ";
							} else {
						 		  $data = "Great, a new data was saved successfully!";
							}
						}
					}
				}
       			 echo json_encode($data);
		
				
			
			}
		}
	}
	public function dltsandAction($id)
	{
		$this->view->setVar('header_title', "Delete Sand");
////////////////////////////////////////////////////////////////
		$sand = Sand::findFirst(array("id='".$id."'"));
		$categ = Sandcateg::findFirst(array("id='".$sand->sandcateg."'"));

        $this->view->sandcateg =$categ->category ;
        $this->view->price = $sand->price ;


        ////update
        if ($this->request->isPost('delete') == true) {

                $dltPhoto = Sand::findFirst('id='.$id.' ');
                $data = array('error' => 'Not Found');
                if ($dltPhoto) {
                    if($dltPhoto->delete()){
                       header('Location: ../sandList');
                    }
                }

        }



		
	}

}
